function GraphSVG(){
	this.dMainAnim=function(mainAnim){
		//SVG
		var res='<?xml version="1.0" standalone="no"?>';
		res=res+'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		res=res+"<svg width='"+mainAnim.resx+"px' height='"+mainAnim.resy+"px' viewBox='0 0 "+mainAnim.resx+" "+mainAnim.resy+"' xmlns='http://www.w3.org/2000/svg'>";
    for(var i=0;i<mainAnim.g.length;i++){
    	res=res+mainAnim.g[i].dibujar();
    }
		res=res+"</svg>";
		return res;
	}
	this.dPunto=function(punto){
		if(punto.std!=2)return;
		return "<circle cx='"+punto.v2[0]+"' cy='"+punto.v2[1]+"' r='1' fill='"+punto.c.rep1+"' stroke='none'/>";
  }
	this.dLinea=function(linea){
    if(linea.std!=2)return "";
		//return "<line x1='"+linea.p1.v2[0]+"' y1='"+linea.p1.v2[1]+"' x2='"+linea.p2.v2[0]+"' y2='"+linea.p2.v2[1]+"' stroke='"+linea.c.rep1+"'/>";
		return "<line x1='"+linea.p1.v2[0]+"' y1='"+linea.p1.v2[1]+"' x2='"+linea.p2.v2[0]+"' y2='"+linea.p2.v2[1]+"' stroke='#000000'/>";
  }
	this.dPoligono=function(poligono){
		if(poligono.std!=2)return;
		var r="<path tipo='dPoligono' fill='"+poligono.cr.rep1()+"' stroke='"+poligono.c.rep1()+"' d='";
		r+="M"+poligono.p[0].v2[0]+" "+poligono.p[0].v2[1];
		for(var i=1;i<poligono.p.length;i++){
			r+=" L"+poligono.p[i].v2[0]+" "+poligono.p[i].v2[1];
		}
		r+=" Z'/>";
		r+=this.dTextura(poligono);
		return r;
	}
	this.dC2Bezier=function(bezier){
		if(bezier.std!=2)return;
		var r="<path tipo='dC2Bezier' stroke='#000000' d='";
		r+="M"+poligono.p[0].v2[0]+" "+poligono.p[0].v2[1];
		r+=" Q"+bezier.pc.v2[0]+" "+bezier.pc.v2[1]+" "+bezier.p2.v2[0]+" "+bezier.p2.v2[1];
		r+=" Z'/>";
		return r;
	}
	this.dCPath=function(cpath){ //(1-t)^2*A+2*t*(1-t)*B+t^2*C B->control ->Bezier cuadrática
		if(cpath.std!=2)return;
		var r="<path tipo='dCPath' fill='"+cpath.cr.rep1()+"' stroke='"+cpath.c.rep1()+"' d='";
		r+="M"+cpath.p[0].v2[0]+" "+cpath.p[0].v2[1];
		for(var i=1;i+1<cpath.p.length;i=i+2){
			if(cpath.p[i]){
				r+="Q"+cpath.p[i].v2[0]+" "+cpath.p[i].v2[1]+" "+cpath.p[i+1].v2[0]+" "+cpath.p[i+1].v2[1];
			}else{
				r+="L"+cpath.p[i+1].v2[0]+" "+cpath.p[i+1].v2[1];
			}
		}
		r+=" Z'/>";
		return r;
	}
	this.dTextura=function(poligono){
		var r="";
		if(poligono.p.length==4&&poligono.textura){
			var textura=poligono.textura;
			for(var i=0;i<textura.elementos.length;i++){
				var ruta=textura.elementos[i];
				if(ruta.elementos.length>0){
					r+="<path tipo='dTextura' fill='"+ruta.c1.rep1()+"' stroke='"+ruta.c2.rep1()+"' d='";
					for(var j=0;j<ruta.elementos.length;j++){
						var elemento=ruta.elementos[j];
						if(elemento instanceof Mover2D){
							var p=conversion2D(elemento.x,elemento.y,textura,poligono.p);
							r+=" M"+p[0]+" "+p[1];
						}
						if(elemento instanceof Linea2D){
							var p=conversion2D(elemento.x,elemento.y,textura,poligono.p);
							r+=" L"+p[0]+" "+p[1];
						}
						if(elemento instanceof Curva2D){
							var c=conversion2D(elemento.cx,elemento.cy,textura,poligono.p);
							var p=conversion2D(elemento.x,elemento.y,textura,poligono.p);
							r+=" Q"+c[0]+" "+c[1]+" "+p[0]+" "+p[1];
						}
					}
					r+=" Z'/>";
				}
			}
		}
		return r;
	}
}
graficador=new GraphSVG();

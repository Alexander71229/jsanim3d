//Elementos de dos dimensiones
//A la Textura se agregan rutas.
//A las ruta se agregan el resto de elementos.
function conversion2D(x,y,textura,puntos){
	//(b-a)t+a;
	var dx=x/textura.w;
	var dy=y/textura.h;
	var h1=[];
	var h2=[];
	for(var k=0;k<2;k++){
		h1[k]=(puntos[1].v2[k]-puntos[0].v2[k])*dx+puntos[0].v2[k];
		h2[k]=(puntos[2].v2[k]-puntos[3].v2[k])*dx+puntos[3].v2[k];
	}
	var r=[];
	for(var k=0;k<2;k++){
		r[k]=(h2[k]-h1[k])*dy+h1[k];
	}
	return r;
}
function Curva2D(cx,cy,x,y){
	this.cx=cx;
	this.cy=cy;
	this.x=x;
	this.y=y;
	this.validar=function(w,h){
		if(this.cx<0||this.cx>w){
			return false;
		}
		if(this.cy<0||this.cy>h){
			return false;
		}
		if(this.x<0||this.x>w){
			return false;
		}
		if(this.y<0||this.y>h){
			return false;
		}
		return true;
	}
}
function Linea2D(x,y){
	this.x=x;
	this.y=y;
	this.validar=function(w,h){
		if(this.x<0||this.x>w){
			return false;
		}
		if(this.y<0||this.y>h){
			return false;
		}
		return true;
	}
}
function Mover2D(x,y){
	this.x=x;
	this.y=y;
	this.validar=function(w,h){
		if(this.x<0||this.x>w){
			return false;
		}
		if(this.y<0||this.y>h){
			return false;
		}
		return true;
	}
}
function Path2D(c1,c2){
	this.c1=c1||new Color(this.ani,[255,255,255]);
	this.c2=c2||new Color(this.ani,[0,0,0]);
	this.elementos=[];
	this.validar=function(w,h){
		for(var i=0;i<this.elementos.length;i++){
			if(!this.elementos[i].validar(w,h)){
				return false;
				break;
			}
		}
		return true;
	}
	this.agregar=function(elemento){
		this.elementos.push(elemento);
	}
}
function Textura(w,h){
	this.w=w;
	this.h=h;
	this.elementos=[];
	this.agregar=function(elemento){
		if(elemento.validar(this.w,this.h)){
			this.elementos.push(elemento);
		}
	}
}
function texturaEjemplo01(){
	var colores=[];
	colores[0]=new Color();
	colores[1]=new Color(null,[245,167,45]);
	colores[2]=new Color(null,[89,45,134]);
	colores[3]=new Color(null,[59,157,56]);
	var textura=new Textura(100,100);
	var ruta=new Path2D(colores[1],colores[2]);
	ruta.agregar(new Mover2D(10,10));
	ruta.agregar(new Linea2D(90,10));
	ruta.agregar(new Linea2D(90,90));
	ruta.agregar(new Linea2D(10,90));
	textura.agregar(ruta);
	return textura;
}
function texturaEjemplo02(){
	var colores=[];
	colores[0]=new Color();
	colores[1]=new Color(null,[245,167,45]);
	colores[2]=new Color(null,[89,45,134]);
	colores[3]=new Color(null,[59,157,56]);
	var textura=new Textura(100,100);
	var ruta=new Path2D(colores[1],colores[2]);
	ruta.agregar(new Mover2D(10,10));
	ruta.agregar(new Linea2D(90,10));
	ruta.agregar(new Linea2D(90,20));
	ruta.agregar(new Curva2D(80,20,70,30));
	ruta.agregar(new Curva2D(60,40,50,40));
	ruta.agregar(new Curva2D(40,40,30,30));
	ruta.agregar(new Curva2D(20,20,10,20));
	textura.agregar(ruta);
	return textura;
}
function crearRectangulo(x,y,w,h,c1,c2){
	var ruta=new Path2D(c1,c2);
	ruta.agregar(new Mover2D(x,y));
	ruta.agregar(new Linea2D(x+w,y));
	ruta.agregar(new Linea2D(x+w,y+h));
	ruta.agregar(new Linea2D(x,y+h));
	return ruta;
}
function crearCirculo(x,y,r,c1,c2){
	var tx=Math.sqrt(2)-1;
	var tg=Math.sqrt(2)/2;
	var ruta=new Path2D(c1,c2);
	ruta.agregar(new Mover2D(x+r,y));
	ruta.agregar(new Curva2D(x+r,y-tx*r,x+tg*r,y-tg*r));
	ruta.agregar(new Curva2D(x+tx*r,y-r,x,y-r));
	ruta.agregar(new Curva2D(x-tx*r,y-r,x-tg*r,y-tg*r));
	ruta.agregar(new Curva2D(x-r,y-tx*r,x-r,y));
	ruta.agregar(new Curva2D(x-r,y+tx*r,x-tg*r,y+tg*r));
	ruta.agregar(new Curva2D(x-tx*r,y+r,x,y+r));
	ruta.agregar(new Curva2D(x-tx*r,y+r,x,y+r));
	ruta.agregar(new Curva2D(x+tx*r,y+r,x+tg*r,y+tg*r));
	ruta.agregar(new Curva2D(x+r,y+tx*r,x+r,y));
	return ruta;
}
function texturaEjemplo03(){ //Círculo
	var colores=[];
	colores[0]=new Color(null,[89,45,255]);
	colores[1]=new Color(null,[255,255,255]);
	var textura=new Textura(100,100);
	textura.agregar(crearCirculo(50,50,30,colores[0],colores[0]));
	return textura;
}
function texturaDado01(c1,c2){ //Dado 1
	var textura=new Textura(300,300);
	textura.agregar(crearRectangulo(5,5,290,290,c2,c2));
	textura.agregar(crearCirculo(150,150,30,c1,c1));
	return textura;
}
function texturaDado02(c1,c2){ //Dado 2
	var textura=new Textura(300,300);
	textura.agregar(crearRectangulo(5,5,290,290,c2,c2));
	textura.agregar(crearCirculo(50,50,30,c1,c1));
	textura.agregar(crearCirculo(250,250,30,c1,c1));
	return textura;
}
function texturaDado03(c1,c2){ //Dado 3
	var textura=new Textura(300,300);
	textura.agregar(crearRectangulo(5,5,290,290,c2,c2));
	textura.agregar(crearCirculo(50,50,30,c1,c1));
	textura.agregar(crearCirculo(150,150,30,c1,c1));
	textura.agregar(crearCirculo(250,250,30,c1,c1));
	return textura;
}
function texturaDado04(c1,c2){ //Dado 4
	var textura=new Textura(300,300);
	textura.agregar(crearRectangulo(5,5,290,290,c2,c2));
	textura.agregar(crearCirculo(50,50,30,c1,c1));
	textura.agregar(crearCirculo(250,50,30,c1,c1));
	textura.agregar(crearCirculo(50,250,30,c1,c1));
	textura.agregar(crearCirculo(250,250,30,c1,c1));
	return textura;
}
function texturaDado05(c1,c2){ //Dado 5
	var textura=new Textura(300,300);
	textura.agregar(crearRectangulo(5,5,290,290,c2,c2));
	textura.agregar(crearCirculo(50,50,30,c1,c1));
	textura.agregar(crearCirculo(250,50,30,c1,c1));
	textura.agregar(crearCirculo(150,150,30,c1,c1));
	textura.agregar(crearCirculo(50,250,30,c1,c1));
	textura.agregar(crearCirculo(250,250,30,c1,c1));
	return textura;
}
function texturaDado06(c1,c2){ //Dado 6
	var textura=new Textura(300,300);
	textura.agregar(crearRectangulo(5,5,290,290,c2,c2));
	textura.agregar(crearCirculo(50,50,30,c1,c1));
	textura.agregar(crearCirculo(250,50,30,c1,c1));
	textura.agregar(crearCirculo(50,150,30,c1,c1));
	textura.agregar(crearCirculo(250,150,30,c1,c1));
	textura.agregar(crearCirculo(50,250,30,c1,c1));
	textura.agregar(crearCirculo(250,250,30,c1,c1));
	return textura;
}
function texturaLadrillo(c1,c2){
	var d=10;
	var d2=20;
	var textura=new Textura(200,200);
	textura.agregar(crearRectangulo(1,0,198,200,c2,c2));
	//textura.agregar(crearRectangulo(0,0,200,103,c2,c2));
	//textura.agregar(crearRectangulo(50,100,100,100,c2,c2));
	textura.agregar(crearRectangulo(d,d,200-d2,100-d2,c1,c1));
	textura.agregar(crearRectangulo(0,100+d,100-d,100-d2,c1,c1));
	textura.agregar(crearRectangulo(100+d,100+d,100-d,100-d2,c1,c1));
	return textura;
}
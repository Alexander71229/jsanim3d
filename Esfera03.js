try{
	m=new MainAnim();
	//m.resx=4000;
	//m.resy=3000;
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[0,0,140]);
	b1.enlazarBase(m.e[1][0]);
	b2=new Base(m,0,300,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	colores[6]=new Color(m,[255,255,255]);
	colores[7]=new Color(m,[0,0,255]);
	
	//Círculo
	var r1=23;
	var r2=25;
	var tx=Math.sqrt(2)-1;
	var tg=Math.sqrt(2)/2;
	var tz=Math.sqrt(3)/3;
	//segmento01
	var puntos=[];
	puntos.push(new Punto(m,0,300,[r2,0,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r2,tx*r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[tg*r2,tg*r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[tg*r1,tg*r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r1,tx*r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[r1,0,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	
	//Esfera
	var r=10;
	//Segmento01
	var vx=[];
	vx.push([0,0,r]);
	vx.push([0,r*tg,r*tg]);
	vx.push([r*tz,r*tz,r*tz]);
	vx.push([r*tg,0,r*tg]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%4])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento02
	var vx=[];
	vx.push([0,r*tg,r*tg]);
	vx.push([r*tz,r*tz,r*tz]);
	vx.push([0,r,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento03
	var vx=[];
	vx.push([r*tg,0,r*tg]);
	vx.push([r*tz,r*tz,r*tz]);
	vx.push([r*tg,r*tg,0]);
	vx.push([r,0,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento04
	var vx=[];
	vx.push([r*tz,r*tz,r*tz]);
	vx.push([0,r,0]);
	vx.push([r*tg,r*tg,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento05
	var vx=[];
	vx.push([0,0,r]);
	vx.push([0,-r*tg,r*tg]);
	vx.push([r*tz,-r*tz,r*tz]);
	vx.push([r*tg,0,r*tg]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%4])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento06
	var vx=[];
	vx.push([0,-r*tg,r*tg]);
	vx.push([r*tz,-r*tz,r*tz]);
	vx.push([0,-r,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento07
	var vx=[];
	vx.push([r*tg,0,r*tg]);
	vx.push([r*tz,-r*tz,r*tz]);
	vx.push([r*tg,-r*tg,0]);
	vx.push([r,0,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento08
	var vx=[];
	vx.push([r*tz,-r*tz,r*tz]);
	vx.push([0,-r,0]);
	vx.push([r*tg,-r*tg,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento09
	var vx=[];
	vx.push([0,0,-r]);
	vx.push([0,r*tg,-r*tg]);
	vx.push([r*tz,r*tz,-r*tz]);
	vx.push([r*tg,0,-r*tg]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%4])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento10
	var vx=[];
	vx.push([0,r*tg,-r*tg]);
	vx.push([r*tz,r*tz,-r*tz]);
	vx.push([0,r,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento11
	var vx=[];
	vx.push([0,0,-r]);
	vx.push([0,-r*tg,-r*tg]);
	vx.push([r*tz,-r*tz,-r*tz]);
	vx.push([r*tg,0,-r*tg]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%4])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento12
	var vx=[];
	vx.push([0,-r*tg,-r*tg]);
	vx.push([r*tz,-r*tz,-r*tz]);
	vx.push([0,-r,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento13
	var vx=[];
	vx.push([r*tg,0,-r*tg]);
	vx.push([r*tz,r*tz,-r*tz]);
	vx.push([r*tg,r*tg,0]);
	vx.push([r,0,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento14
	var vx=[];
	vx.push([r*tz,r*tz,-r*tz]);
	vx.push([0,r,0]);
	vx.push([r*tg,r*tg,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento15
	var vx=[];
	vx.push([r*tg,0,-r*tg]);
	vx.push([r*tz,-r*tz,-r*tz]);
	vx.push([r*tg,-r*tg,0]);
	vx.push([r,0,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);
	//Segmento16
	var vx=[];
	vx.push([r*tz,-r*tz,-r*tz]);
	vx.push([0,-r,0]);
	vx.push([r*tg,-r*tg,0]);
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
		puntos.push(new Punto(m,0,300,per(vx[i],vx[(i+1)%vx.length])));//control
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,colores[6],colores[7]);
	b1.agregar(c);

	a=new Anim(m,2,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,2,[m.rad*45,m.rad*0,m.rad*0],[0,0,140]);
	b1.agregarAnim(a);
	a=new Anim(m,2,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=1.99){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
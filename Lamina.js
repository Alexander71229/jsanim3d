//Liber�a
//Los puntos son vectores simples
//las excluciones son un vector de vectores de dos dimensiones
function lamina(mainAnim,base,ti,tf,c1,c2,c3,punto1,punto2,punto3,punto4,division1,division2,exclusiones,textura){
	var m=mainAnim;
	hash={};
	try{
		if(exclusiones&&exclusiones.length){
			for(var i=0;i<exclusiones.length;i++){
				hash[exclusiones[i][0]+":"+exclusiones[i][1]]=true;
			}
		}
	}catch(e){
	}
	hx={};
	var kx="";
	for(var i=0;i<division1;i++){
		kx=i+":p2p1";
		if(hx[kx]){
			var p1=hx[kx];
		}else{
			var p1=[];
			for(var k=0;k<3;k++){
				p1[k]=i*(punto2[k]-punto1[k])/division1+punto1[k];
			}
			hx[kx]=p1;
		}
		kx=i+":p3p4";
		if(hx[kx]){
			var p2=hx[kx];
		}else{
			var p2=[];
			for(var k=0;k<3;k++){
				p2[k]=i*(punto3[k]-punto4[k])/division1+punto4[k];
			}
			hx[kx]=p2;
		}
		kx=(i+1)+":p2p1";
		if(hx[kx]){
			p3=hx[kx];
		}else{
			var p3=[];
			for(var k=0;k<3;k++){
				p3[k]=(i+1)*(punto2[k]-punto1[k])/division1+punto1[k];
			}
			hx[kx]=p3;
		}
		kx=(i+1)+":p3p4";
		if(hx[kx]){
			var p4=hx[kx];
		}else{
			var p4=[];
			for(var k=0;k<3;k++){
				p4[k]=(i+1)*(punto3[k]-punto4[k])/division1+punto4[k];
			}
			hx[kx]=p4;
		}
		for(var j=0;j<division2;j++){
			if(!hash[i+":"+j]){
				kx=j+":p2p1k"+i;
				if(hx[kx]){
					var q1=hx[kx];
				}else{
					var q1=[];
					for(var k=0;k<3;k++){
						q1[k]=j*(p2[k]-p1[k])/division2+p1[k];
					}
					hx[kx]=q1;
				}
				kx=j+":p4p3k"+i;
				if(hx[kx]){
					var q2=hx[kx];
				}else{
					var q2=[];
					for(var k=0;k<3;k++){
						q2[k]=j*(p4[k]-p3[k])/division2+p3[k];
					}
					hx[kx]=q2;
				}
				kx=(j+1)+":p2p1k"+i;
				if(hx[kx]){
					var q3=hx[kx];
				}else{
					var q3=[];
					for(var k=0;k<3;k++){
						q3[k]=(j+1)*(p2[k]-p1[k])/division2+p1[k];
					}
					hx[kx]=q3;
				}
				kx=(j+1)+":p4p3k"+i;
				if(hx[kx]){
					var q4=hx[kx];
				}else{
					var q4=[];
					for(var k=0;k<3;k++){
						q4[k]=(j+1)*(p4[k]-p3[k])/division2+p3[k];
					}
					hx[kx]=q4;
				}
				var puntos=[];
				puntos.push(new Punto(m,ti,tf,[q1[0],q1[1],q1[2]],[0,0,0]));
				puntos.push(new Punto(m,ti,tf,[q2[0],q2[1],q2[2]],[0,0,0]));
				puntos.push(new Punto(m,ti,tf,[q4[0],q4[1],q4[2]],[0,0,0]));
				puntos.push(new Punto(m,ti,tf,[q3[0],q3[1],q3[2]],[0,0,0]));
				c=new Poligono(m,ti,tf,puntos,c1,c2);
				c.textura=textura;
				base.agregar(c);
				if(c3){
					if(j==0){
						cx=new Linea(m,ti,tf,q1,q2,c3);
						cx.antes=c;
						cx.gZ=function(){return this.antes.gZ()-.0000001};
						base.agregar(cx);
					}
					if(i==0){
						cx=new Linea(m,ti,tf,q1,q3,c3);
						cx.antes=c;
						cx.gZ=function(){return this.antes.gZ()-.0000001};
						base.agregar(cx);
					}
					if(j==(division2-1)){
						cx=new Linea(m,ti,tf,q3,q4,c3);
						cx.antes=c;
						cx.gZ=function(){return this.antes.gZ()-.0000001};
						base.agregar(cx);
					}
					if(i==(division1-1)){
						cx=new Linea(m,ti,tf,q2,q4,c3);
						cx.antes=c;
						cx.gZ=function(){return this.antes.gZ()-.0000001};
						base.agregar(cx);
					}
				}
			}
		}
	}
}
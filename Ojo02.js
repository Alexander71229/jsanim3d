try{
	m=new MainAnim();
	//m.resx=4000;
	//m.resy=3000;
	b0=new Base(m,0,300,[m.rad*(180+45),m.rad*0,m.rad*0],[0,0,180]);
	b0.enlazarBase(m.e[1][0]);
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[25,0,0]);
	b1.enlazarBase(b0);
	b3=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-25,0,0]);
	b3.enlazarBase(b0);
	b2=new Base(m,0,300,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	colores[6]=new Color(m,[255,255,255]);
	colores[7]=new Color(m,[0,0,255]);
	
	//Ojo
	var r=20;
	var r1=10;
	var r2=9;
	var r3=3;
	//iri(r,r1,r2,b1,colores[1],colores[1]);
	//iri(r,r2,r3,b1,colores[2],colores[2]);
	//iri2(r,r3,b1,colores[3],colores[3]);
	iri(r,19,15,b1);
	//iri(r,15,10,b1);

	//iri(r,r1,r2,b3,colores[1],colores[1]);
	//iri(r,r2,r3,b3,colores[2],colores[2]);
	//iri2(r,r3,b3,colores[3],colores[3]);
	iri(r,19,15,b3);
	//iri(r,15,10,b3);
	//iri(r,20,19,b3);
	r=21;
	var vx=[];
	vx.push(new Punto(m,0,300,[15,0,circunZ(15,0,r)]));
	vx.push(new Punto(m,0,300,per([15,0,circunZ(15,0,r)],[10,-5,circunZ(10,-5,r)])));
	vx.push(new Punto(m,0,300,[10,-5,circunZ(10,-5,r)]));
	vx.push(new Punto(m,0,300,per([10,-5,circunZ(10,-5,r)],[5,-10,circunZ(5,-10,r)])));
	vx.push(new Punto(m,0,300,[5,-10,circunZ(5,-10,r)]));
	vx.push(new Punto(m,0,300,per([5,-10,circunZ(5,-10,r)],[0,-10,circunZ(0,-10,r)])));
	vx.push(new Punto(m,0,300,[0,-10,circunZ(0,-10,r)]));
	vx.push(null);
	c=new CPath(m,0,3,vx);
	//b3.agregar(c);


	a=new Anim(m,10,[m.rad*(180+0),m.rad*0,m.rad*0],[0,0,180]);
	b0.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[25,0,0]);
	b1.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-25,0,0]);
	b3.agregarAnim(a);
	
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=5.99){
			setTimeout(function(){dibujar(i+.1)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
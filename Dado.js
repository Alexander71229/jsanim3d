try{
	m=new MainAnim();
	//m.resx=4000;
	//m.resy=3000;
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[0,0,140]);
	b1.enlazarBase(m.e[1][0]);
	b2=new Base(m,0,300,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	colores[6]=new Color(m,[255,255,255]);
	colores[7]=new Color(m,[0,0,255]);
	var parametros=[];
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[1],1,1,null,texturaEjemplo03()));
	parametros.push(new ParametrosLamina(colores[1],colores[6],colores[1],1,1,null,texturaDado03(colores[6],colores[7])));
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[1],1,1,null,texturaEjemplo03()));
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[1],1,1,null,texturaEjemplo03()));
	parametros.push(new ParametrosLamina(colores[1],colores[6],colores[1],1,1,null,texturaDado04(colores[6],colores[7])));
	parametros.push(new ParametrosLamina(colores[1],colores[6],colores[1],1,1,null,texturaDado02(colores[6],colores[7])));
	caja(m,b,0,600,0,0,0,20,20,20,[0,2,3],parametros,null);

	//cruz
	/*c=new Linea(m,0,300,new Punto(m,0,30,[-1,0,0]),new Punto(m,0,30,[1,0,0]),false);
	b2.agregar(c);
	c=new Linea(m,0,300,new Punto(m,0,30,[0,-1,0]),new Punto(m,0,30,[0,1,0]),false);
	b2.agregar(c);
	c=new Linea(m,0,300,new Punto(m,0,30,[0,0,-1]),new Punto(m,0,30,[0,0,1]),false);
	b2.agregar(c);*/

	//Círculo
	var r1=23;
	var r2=25;
	var tx=Math.sqrt(2)-1;
	var tg=Math.sqrt(2)/2;
	//segmento01
	var puntos=[];
	puntos.push(new Punto(m,0,300,[r2,0,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r2,tx*r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[tg*r2,tg*r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[tg*r1,tg*r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r1,tx*r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[r1,0,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	//segmento02
	var puntos=[];
	puntos.push(new Punto(m,0,300,[tg*r2,tg*r2,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[tx*r2,r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[0,r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[0,r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[tx*r1,r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[tg*r1,tg*r1,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	//segmento03
	var puntos=[];
	puntos.push(new Punto(m,0,300,[0,r2,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-tx*r2,r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[-tg*r2,tg*r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[-tg*r1,tg*r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-tx*r1,r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[0,r1,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	//segmento04
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-tg*r2,tg*r2,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-r2,tx*r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[-r2,0,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[-r1,0,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-r1,tx*r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[-tg*r1,tg*r1,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	//segmento05
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-r2,0,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-r2,-tx*r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[-tg*r2,-tg*r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[-tg*r1,-tg*r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-r1,-tx*r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[-r1,0,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	//segmento06
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-tg*r2,-tg*r2,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-tx*r2,-r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[0,-r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[0,-r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-tx*r1,-r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[-tg*r1,-tg*r1,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	//segmento07
	var puntos=[];
	puntos.push(new Punto(m,0,300,[0,-r2,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[tx*r2,-r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[tg*r2,-tg*r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[tg*r1,-tg*r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[tx*r1,-r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[0,-r1,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	//segmento08
	var puntos=[];
	puntos.push(new Punto(m,0,300,[tg*r2,-tg*r2,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r2,-tx*r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[r2,0,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[r1,0,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r1,-tx*r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[tg*r1,-tg*r1,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[0,0,140]);
	b1.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		var svgx=m.dibujar();
		console.log(svgx);
		document.getElementsByTagName('div')[0].innerHTML=svgx;
		m.reset();
		if(i<m.d&&i<=9){
			//setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	console.log(e+":"+e.stack);
}
function Orto(ani,ti,tf,x,y,z,c){
  if(window.ais){
    ais++;
    this.aid=ais;
  }else{
    window.ais=1;
    this.aid=1;
  }
  window["elemento"+this.aid]=this;
  this.tipox="Orto";

	this.agregar=function(b){
    for(var i=0;i<this.pn.length;i++){
    	this.pn[i].agregar(b);
    }
  }
	this.calcular=function(){
    for(var i=0;i<this.pn.length;i++){
    	this.pn[i].calcular();
    }
  }
//Exportaci�n a gr�fico
	this.dibujar=function(){
  	var res="";
    for(var i=0;i<this.pn.length;i++){
    	res=res+this.pn[i].dibujar();
    }
  	return res;
  }
	this.t=6; //tipo de elemento
	this.ani=ani;
	this.std=0; //Estado de los c�lculos
	this.ti=ti||0; //tiempo inicial
	this.tf=tf||0; //Tiempo final
	this.d=0; //Duraci�n
	this.b=0; //id base
	this.id=0; //Identificador �nico
	this.x=x||1; //Ancho
	this.y=y||1; //Alto
	this.z=z||1; //Largo
	x=this.x;
	y=this.y;
	z=this.z;
	this.c=c||new Color(this.ani,[0,0,0]); //Color
	this.pn=[]; //Array de pol�gonos;
	this.pn.push(new Poligono(this.ani,this.ti,this.tf,[[0,0,0],[x,0,0],[x,y,0],[0,y,0]],this.c));
	this.pn.push(new Poligono(this.ani,this.ti,this.tf,[[0,0,z],[x,0,z],[x,y,z],[0,y,z]],this.c));
	this.pn.push(new Poligono(this.ani,this.ti,this.tf,[[0,0,0],[0,0,z],[0,y,z],[0,0,z]],this.c));
	this.pn.push(new Poligono(this.ani,this.ti,this.tf,[[x,0,0],[x,0,z],[x,y,z],[x,0,z]],this.c));
	this.pn.push(new Poligono(this.ani,this.ti,this.tf,[[0,0,0],[x,0,0],[x,0,z],[0,0,z]],this.c));
	this.pn.push(new Poligono(this.ani,this.ti,this.tf,[[0,y,0],[x,y,0],[x,y,z],[0,y,z]],this.c));
	this.ani.all.push(this);
}
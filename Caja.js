//Un ortoedro conformado de 6 l�minas
//0 superior
//5 inferior
function ParametrosLamina(c1,c2,c3,division1,division2,exclusiones,textura){
	this.c1=c1;
	this.c2=c2;
	this.c3=c3;
	this.division1=division1;
	this.division2=division2;
	this.exclusiones=exclusiones;
	this.textura=textura;
}
//Creaci�n de cajas dentro de cajas
function ParametroCajaInterna(cara,x,y,exclusiones,parametros,cajasInternas,longitud){
	this.cara=cara;
	this.x=x;
	this.y=y;
	this.exclusiones=exclusiones;
	this.parametros=parametros;
	this.cajasInternas=cajasInternas;
	this.longitud=longitud;
}
function caja(mainAnim,base,ti,tf,x,y,z,w,h,l,exclusiones,parametros,cajasInternas){
	var puntos=[];
	puntos.push([x,y,z]);
	puntos.push([x+w,y,z]);
	puntos.push([x+w,y,z+l]);
	puntos.push([x,y,z+l]);
	puntos.push([x,y+h,z]);
	puntos.push([x+w,y+h,z]);
	puntos.push([x+w,y+h,z+l]);
	puntos.push([x,y+h,z+l]);
	var hash={};
	if(exclusiones){
		for(var i=0;i<exclusiones.length;i++){
			hash[exclusiones[i]]=true;
		}
	}
	var internas=[];
	for(var i=0;i<6;i++){
		internas[i]=[];
	}
	if(cajasInternas&&cajasInternas.length){
		for(var i=0;i<cajasInternas.length;i++){
			internas[cajasInternas[i].cara].push(cajasInternas[i]);
		}
	}
	var i=0;
	if(!hash[i]){
		lamina(mainAnim,base,ti,tf,parametros[i].c1,parametros[i].c2,parametros[i].c3,puntos[0],puntos[1],puntos[2],puntos[3],parametros[i].division1,parametros[i].division2,parametros[i].exclusiones,parametros[i].textura);
	}
	for(var j=0;j<internas[i].length;j++){
		var dimension=h;
		if(internas[i][j].longitud){
			dimension=internas[i][j].longitud;
		}
		var dx=(w)/parametros[i].division1;
		var dy=(l)/parametros[i].division2;
		caja(mainAnim,base,ti,tf,x+dx*internas[i][j].x,y,z+dy*internas[i][j].y,dx,dimension,dy,internas[i][j].exclusiones,internas[i][j].parametros,internas[i][j].cajasInternas);
	}
	i=1;
	if(!hash[i]){
		lamina(mainAnim,base,ti,tf,parametros[i].c1,parametros[i].c2,parametros[i].c3,puntos[0],puntos[1],puntos[5],puntos[4],parametros[i].division1,parametros[i].division2,parametros[i].exclusiones,parametros[i].textura);
	}
	for(var j=0;j<internas[i].length;j++){
		var dimension=l;
		if(internas[i][j].longitud){
			dimension=internas[i][j].longitud;
		}
		var dx=(w)/parametros[i].division1;
		var dy=(h)/parametros[i].division2;
		caja(mainAnim,base,ti,tf,x+dx*internas[i][j].x,y+dy*internas[i][j].y,z,dx,dy,dimension,internas[i][j].exclusiones,internas[i][j].parametros,internas[i][j].cajasInternas);
	}
	i=2;
	if(!hash[i]){
		lamina(mainAnim,base,ti,tf,parametros[i].c1,parametros[i].c2,parametros[i].c3,puntos[1],puntos[2],puntos[6],puntos[5],parametros[i].division1,parametros[i].division2,parametros[i].exclusiones,parametros[i].textura);
	}
	for(var j=0;j<internas[i].length;j++){
		var dimension=w;
		if(internas[i][j].longitud){
			dimension=internas[i][j].longitud;
		}
		var dx=(l)/parametros[i].division1;
		var dy=(h)/parametros[i].division2;
		caja(mainAnim,base,ti,tf,x+w-dimension,y+dy*internas[i][j].y,z+dx*internas[i][j].x,dimension,dy,dx,internas[i][j].exclusiones,internas[i][j].parametros,internas[i][j].cajasInternas);
	}
	i=3;
	if(!hash[i]){
		lamina(mainAnim,base,ti,tf,parametros[i].c1,parametros[i].c2,parametros[i].c3,puntos[2],puntos[3],puntos[7],puntos[6],parametros[i].division1,parametros[i].division2,parametros[i].exclusiones,parametros[i].textura);
	}
	for(var j=0;j<internas[i].length;j++){
		var dimension=l;
		if(internas[i][j].longitud){
			dimension=internas[i][j].longitud;
		}
		var dx=(w)/parametros[i].division1;
		var dy=(h)/parametros[i].division2;
		caja(mainAnim,base,ti,tf,x+dx*internas[i][j].x,y+dy*internas[i][j].y,z+l-dimension,dx,dy,dimension,internas[i][j].exclusiones,internas[i][j].parametros,internas[i][j].cajasInternas);
	}
	i=4;
	if(!hash[i]){
		lamina(mainAnim,base,ti,tf,parametros[i].c1,parametros[i].c2,parametros[i].c3,puntos[3],puntos[0],puntos[4],puntos[7],parametros[i].division1,parametros[i].division2,parametros[i].exclusiones,parametros[i].textura);
	}
	for(var j=0;j<internas[i].length;j++){
		var dimension=w;
		if(internas[i][j].longitud){
			dimension=internas[i][j].longitud;
		}
		var dx=(l)/parametros[i].division1;
		var dy=(h)/parametros[i].division2;
		caja(mainAnim,base,ti,tf,x,y+dy*internas[i][j].y,z+dx*internas[i][j].x,dimension,dy,dx,internas[i][j].exclusiones,internas[i][j].parametros,internas[i][j].cajasInternas);
	}
	i=5;
	if(!hash[i]){
		lamina(mainAnim,base,ti,tf,parametros[i].c1,parametros[i].c2,parametros[i].c3,puntos[4],puntos[5],puntos[6],puntos[7],parametros[i].division1,parametros[i].division2,parametros[i].exclusiones,parametros[i].textura);
	}
	for(var j=0;j<internas[i].length;j++){
		var dimension=h;
		if(internas[i][j].longitud){
			dimension=internas[i][j].longitud;
		}
		var dx=(w)/parametros[i].division1;
		var dy=(l)/parametros[i].division2;
		caja(mainAnim,base,ti,tf,x+dx*internas[i][j].x,y+h-dimension,z+dy*internas[i][j].y,dx,dimension,dy,internas[i][j].exclusiones,internas[i][j].parametros,internas[i][j].cajasInternas);
	}
}
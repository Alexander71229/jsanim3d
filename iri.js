d45=Math.sqrt(2)/2;
function iri(r,r1,r2,b1,c1,c2){
	var z1=circunZ(r1,0,r);
	var z2=circunZ(r2,0,r);
	//A01
	var vx=[];
	vx.push([r1,0,z1]);
	vx.push(per([r1,0,z1],[r2,0,z2]));
	vx.push([r2,0,z2]);
	vx.push(perd([r2,0,z2],[r2*d45,r2*d45,z2],[0,0,z2]));
	vx.push([r2*d45,r2*d45,z2]);
	vx.push(per([r2*d45,r2*d45,z2],[r1*d45,r1*d45,z1]));
	vx.push([r1*d45,r1*d45,z1]);
	vx.push(perd([r1*d45,r1*d45,z1],[r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A02
	var vx=[];
	vx.push([r1*d45,r1*d45,z1]);
	vx.push(per([r1*d45,r1*d45,z1],[r2*d45,r2*d45,z2]));
	vx.push([r2*d45,r2*d45,z2]);
	vx.push(perd([r2*d45,r2*d45,z2],[0,r2,z2],[0,0,z2]));
	vx.push([0,r2,z2]);
	vx.push(per([0,r2,z2],[0,r1,z1]));
	vx.push([0,r1,z1]);
	vx.push(perd([0,r1,z1],[r1*d45,r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A03
	var vx=[];
	vx.push([-r1,0,z1]);
	vx.push(per([-r1,0,z1],[-r2,0,z2]));
	vx.push([-r2,0,z2]);
	vx.push(perd([-r2,0,z2],[-r2*d45,r2*d45,z2],[0,0,z2]));
	vx.push([-r2*d45,r2*d45,z2]);
	vx.push(per([-r2*d45,r2*d45,z2],[-r1*d45,r1*d45,z1]));
	vx.push([-r1*d45,r1*d45,z1]);
	vx.push(perd([-r1*d45,r1*d45,z1],[-r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A04
	var vx=[];
	vx.push([-r1*d45,r1*d45,z1]);
	vx.push(per([-r1*d45,r1*d45,z1],[-r2*d45,r2*d45,z2]));
	vx.push([-r2*d45,r2*d45,z2]);
	vx.push(perd([-r2*d45,r2*d45,z2],[0,r2,z2],[0,0,z2]));
	vx.push([0,r2,z2]);
	vx.push(per([0,r2,z2],[0,r1,z1]));
	vx.push([0,r1,z1]);
	vx.push(perd([0,r1,z1],[-r1*d45,r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A05
	var vx=[];
	vx.push([-r1,0,z1]);
	vx.push(per([-r1,0,z1],[-r2,0,z2]));
	vx.push([-r2,0,z2]);
	vx.push(perd([-r2,0,z2],[-r2*d45,-r2*d45,z2],[0,0,z2]));
	vx.push([-r2*d45,-r2*d45,z2]);
	vx.push(per([-r2*d45,-r2*d45,z2],[-r1*d45,-r1*d45,z1]));
	vx.push([-r1*d45,-r1*d45,z1]);
	vx.push(perd([-r1*d45,-r1*d45,z1],[-r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A06
	var vx=[];
	vx.push([-r1*d45,-r1*d45,z1]);
	vx.push(per([-r1*d45,-r1*d45,z1],[-r2*d45,-r2*d45,z2]));
	vx.push([-r2*d45,-r2*d45,z2]);
	vx.push(perd([-r2*d45,-r2*d45,z2],[0,-r2,z2],[0,0,z2]));
	vx.push([0,-r2,z2]);
	vx.push(per([0,-r2,z2],[0,-r1,z1]));
	vx.push([0,-r1,z1]);
	vx.push(perd([0,-r1,z1],[-r1*d45,-r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A07
	var vx=[];
	vx.push([r1,0,z1]);
	vx.push(per([r1,0,z1],[r2,0,z2]));
	vx.push([r2,0,z2]);
	vx.push(perd([r2,0,z2],[r2*d45,-r2*d45,z2],[0,0,z2]));
	vx.push([r2*d45,-r2*d45,z2]);
	vx.push(per([r2*d45,-r2*d45,z2],[r1*d45,-r1*d45,z1]));
	vx.push([r1*d45,-r1*d45,z1]);
	vx.push(perd([r1*d45,-r1*d45,z1],[r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A08
	var vx=[];
	vx.push([r1*d45,-r1*d45,z1]);
	vx.push(per([r1*d45,-r1*d45,z1],[r2*d45,-r2*d45,z2]));
	vx.push([r2*d45,-r2*d45,z2]);
	vx.push(perd([r2*d45,-r2*d45,z2],[0,-r2,z2],[0,0,z2]));
	vx.push([0,-r2,z2]);
	vx.push(per([0,-r2,z2],[0,-r1,z1]));
	vx.push([0,-r1,z1]);
	vx.push(perd([0,-r1,z1],[r1*d45,-r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
}
function iri2(r,r1,b1,c1,c2){
	var z1=circunZ(r1,0,r);
	//A01
	var vx=[];
	vx.push([r1,0,z1]);
	vx.push(per([r1,0,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[r1*d45,r1*d45,z1]));
	vx.push([r1*d45,r1*d45,z1]);
	vx.push(perd([r1*d45,r1*d45,z1],[r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A02
	var vx=[];
	vx.push([r1*d45,r1*d45,z1]);
	vx.push(per([r1*d45,r1*d45,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[0,r1,z1]));
	vx.push([0,r1,z1]);
	vx.push(perd([0,r1,z1],[r1*d45,r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A03
	var vx=[];
	vx.push([-r1,0,z1]);
	vx.push(per([-r1,0,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[-r1*d45,r1*d45,z1]));
	vx.push([-r1*d45,r1*d45,z1]);
	vx.push(perd([-r1*d45,r1*d45,z1],[-r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A04
	var vx=[];
	vx.push([-r1*d45,r1*d45,z1]);
	vx.push(per([-r1*d45,r1*d45,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[0,r1,z1]));
	vx.push([0,r1,z1]);
	vx.push(perd([0,r1,z1],[-r1*d45,r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A05
	var vx=[];
	vx.push([-r1,0,z1]);
	vx.push(per([-r1,0,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[-r1*d45,-r1*d45,z1]));
	vx.push([-r1*d45,-r1*d45,z1]);
	vx.push(perd([-r1*d45,-r1*d45,z1],[-r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A06
	var vx=[];
	vx.push([-r1*d45,-r1*d45,z1]);
	vx.push(per([-r1*d45,-r1*d45,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[0,-r1,z1]));
	vx.push([0,-r1,z1]);
	vx.push(perd([0,-r1,z1],[-r1*d45,-r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A07
	var vx=[];
	vx.push([r1,0,z1]);
	vx.push(per([r1,0,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[r1*d45,-r1*d45,z1]));
	vx.push([r1*d45,-r1*d45,z1]);
	vx.push(perd([r1*d45,-r1*d45,z1],[r1,0,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
	//A08
	var vx=[];
	vx.push([r1*d45,-r1*d45,z1]);
	vx.push(per([r1*d45,-r1*d45,z1],[0,0,r]));
	vx.push([0,0,r]);
	vx.push(per([0,0,r],[0,-r1,z1]));
	vx.push([0,-r1,z1]);
	vx.push(perd([0,-r1,z1],[r1*d45,-r1*d45,z1],[0,0,z1]));
	var puntos=[];
	for(var i=0;i<vx.length;i++){
		puntos.push(new Punto(m,0,300,vx[i]));
	}
	puntos.push(new Punto(m,0,300,vx[0]));
	c=new CPath(m,0,3,puntos,c1,c2);
	b1.agregar(c);
}
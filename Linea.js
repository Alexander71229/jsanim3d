function Linea(ani,ti,tf,p1,p2,c){
  if(window.ais){
    ais++;
    this.aid=ais;
  }else{
    window.ais=1;
    this.aid=1;
  }
  window["elemento"+this.aid]=this;
  this.tipox="Linea";

	this.agregar=function(b){
		this.p1.agregar(b);
		this.p2.agregar(b);
  }
//Ajustar un segmento de recta con otro que sea completamente visible pero que sea representativo
//Temporalmente incompleta, falta cortar x y y
	this.segmentar=function(){
		var l=this;
    if(l.std==0){
    	var t;
      if(l.p1.t3[2]<this.ani.zmin&&l.p2.t3[2]<this.ani.zmin){
				//ningun punto es visible: la linea no se debe dibujar
				return 3;
      }else{
        if(this.p1.t3[2]>=this.ani.zmin){
          if(this.p2.t3[2]<this.ani.zmin){
						//El punto 1 es visible en Z y el punto 2 no es visible en Z
						t=(this.ani.zmin-this.p1.t3[2])/(this.p2.t3[2]-this.p1.t3[2]);
						this.p2.t3[0]=(this.p2.t3[0]-this.p1.t3[0])*t+this.p1.t3[0];
						this.p2.t3[1]=(this.p2.t3[1]-this.p1.t3[1])*t+this.p1.t3[1];
						this.p2.t3[2]=this.ani.zmin;
          }else{
          	//Ambos son visibles en Z
          }
        }else{
          if(this.p2.t3[2]>=this.ani.zmin){
						//El punto 1 no es visible en Z y 2 es visible en Z
						t=(this.ani.zmin-this.p1.t3[2])/(this.p2.t3[2]-this.p1.t3[2]);
						this.p2.t3[0]=(this.p2.t3[0]-this.p1.t3[0])*t+this.p1.t3[0];
						this.p2.t3[1]=(this.p2.t3[1]-this.p1.t3[1])*t+this.p1.t3[1];
						this.p2.t3[2]=this.zmin;
          }
        }
      }
			//Codigo para segmentar en x y y, a�n incompleto
			this.std=1;
			this.p1.std=1;
			this.p2.std=1;
    }
  }
	this.calcular=function(){
		this.p1.calcular();
		this.p2.calcular();
		this.zO=this.gZ();
	  this.segmentar();
    if(this.std!=1)return 0;
		this.p1.mapeo();
		this.p2.mapeo();
		this.std=2;
		return 1;
  }
//Exportaci�n a gr�fico
	this.dibujar=function(){
		return graficador.dLinea(this);
  }
	this.gZ=function(){
		return (this.p1.gD()+this.p2.gD())/2;
	}
	this.t=5; //tipo de elemento
	this.ani=ani;
	this.std=0; //Estado de los c�lculos
	this.ti=ti||0; //tiempo inicial
	this.tf=tf||0; //Tiempo final
	this.d=0; //Duraci�n
	this.b=null; //id base
	this.id=0; //Identificador �nico
	if(p1.length){//Usa un vector de 3 dimensiones para indicar las coordenadas
		p1=new Punto(ani,ti,tf,p1);
	}
	if(p2.length){//Usa un vector de 3 dimensiones para indicar las coordenadas
		p2=new Punto(ani,ti,tf,p2);
	}
	this.p1=p1; //Punto1
	this.p2=p2; //Punto2
	this.c=c||new Color(this.ani,[0,0,0]); //Color
	this.ani.all.push(this);
}
try{
	m=new MainAnim();
	b0=new Base(m,0,300,[m.rad*180,m.rad*0,m.rad*0],[0,0,180]);
	b0.enlazarBase(m.e[1][0]);
	b1=new Base(m,0,300,[m.rad*7,m.rad*0,m.rad*0],[17,0,0]);
	b1.enlazarBase(b0);
	b2=new Base(m,0,300,[m.rad*-7,m.rad*0,m.rad*0],[-17,0,0]);
	b2.enlazarBase(b0);
	//b1.agregar(new Linea(m,0,300,new Punto(m,0,300,[0,0,0]),new Punto(m,0,300,[0,1,0])));
	var r=10;
	iri(r,5,1,b1);
	iri(r,r,r-1,b1);
	iri(r,5,1,b2);
	iri(r,r,r-1,b2);
	b0.agregarAnim(new Anim(m,1,[m.rad*150,m.rad*0,m.rad*0],[0,0,180]));
	b1.agregarAnim(new Anim(m,300,[m.rad*0,m.rad*0,m.rad*0],[25,0,0]));
	b2.agregarAnim(new Anim(m,300,[m.rad*0,m.rad*0,m.rad*0],[-25,0,0]));
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=5.99){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
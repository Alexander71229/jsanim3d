function AGC(m){
	var h=15;
	var r=Math.floor(250-Math.random()*h);
	var g=Math.floor(250-Math.random()*h);
	var b=Math.floor(250-Math.random()*h);
	return new Color(m,[r,g,b]);
}
function print(m){
	document.getElementById('resultado').value+='\n'+m;
}
function limpiar(){
	document.getElementById('resultado').value='';
}
function mxm(a,b){
	var r=[];
	for(var i=0;i<3;i++){
		r[i]=[];
		for(var j=0;j<3;j++){
			var totm=0;
			for(var k=0;k<3;k++){
				totm=totm+a[k][j]*b[i][k];
			}
			r[i][j]=totm;
		}
	}
	return r;
}
function mxp(m,p){
	var r=[];
	for(var j=0;j<3;j++){
		var totm=0;
		for(var k=0;k<3;k++){
			totm=totm+m[k][j]*p[k];
		}
		r[j]=totm;
	}
	return r;
}
function hex(v){
	if(v<16){
		return "0"+v.toString(16);
	}
	return v.toString(16);
}
function d2d(a,b){
	return Math.sqrt((a.v2[0]-b.v2[0])*(a.v2[0]-b.v2[0])+(a.v2[1]-b.v2[1])*(a.v2[1]-b.v2[1]));
}
function fzbufp(ani,o){
	var p=[];
	for(var i=0;i<o.p.length;i++){
		if(o.p[i]!=null){
			p.push(o.p[i]);
		}
	}
	fzbufpx(ani,p,o);
}
function fzbufpx(ani,ps,o){
	if(ps.length<3){
		alert('No es un polígono');
		return;
	}
	fzbuf(ani,[ps[0],ps[1],ps[ps.length-1]],o);
	var k=3;
	var ans=[ps[1],ps[ps.length-1]];
	var i=2;
	var j=2;
	while(true){
		fzbuf(ani,[ps[i],ans[0],ans[1]],o);
		ans=[ans[1],ps[i]];
		k++;
		i++;
		if(k>=ps.length){
			return;
		}
		fzbuf(ani,[ps[ps.length-j],ans[0],ans[1]],o);
		ans=[ans[1],ps[ps.length-j]];
		k++;
		j++;
		if(k>=ps.length){
			return;
		}
	}
}
function fzbuf(ani,ps,o){
	if(ps.length!=3){
		alert('Error fatal en ZBuf');
		return;
	}
	if(!ani.zbuf){
		ani.zbuf={};
		ani.zbwn=1;
		ani.zbdw=Math.round(ani.resx/ani.zbwn);
		ani.zbwn=Math.ceil(ani.resy/ani.zbdw);
		ani.zbdh=ani.zbdw;
		ani.zbhn=Math.ceil(ani.resy/ani.zbdh);
	}
	/*var dmx2=0;
	var imx2=0;
	for(var i=0;i<3;i++){
		var td2=d2d(ps[i],ps[(i+1)%3]);
		if(dmx2<td2){
			dmx2=td2;
			imx2=i;
		}
	}*/
	if(Math.abs(ps[0].v2[0]-ps[1].v2[0])>Math.abs(ps[0].v2[1]-ps[1].v2[1])){ //Se asume que ani.zbdw y ani.zbdh son iguales
		var di=Math.round(Math.abs(ps[0].v2[0]-ps[1].v2[0])/ani.zbdw)+1;
	}else{
		var di=Math.round(Math.abs(ps[0].v2[1]-ps[1].v2[1])/ani.zbdh)+1;
	}
	if(Math.abs(ps[0].v2[0]-ps[2].v2[0])>Math.abs(ps[0].v2[1]-ps[2].v2[1])){
		var dj=Math.round(Math.abs(ps[0].v2[0]-ps[2].v2[0])/ani.zbdw)+1;
	}else{
		var dj=Math.round(Math.abs(ps[0].v2[1]-ps[2].v2[1])/ani.zbdh)+1;
	}
	if(!o.zbf){
		o.zbf=0;
	}
	var dls=[];
	for(var i=0;i<=di;i++){
		var px=(ps[1].t3[0]-ps[0].t3[0])*(i/di)+ps[0].t3[0];
		var py=(ps[1].t3[1]-ps[0].t3[1])*(i/di)+ps[0].t3[1];
		var pz=(ps[1].t3[2]-ps[0].t3[2])*(i/di)+ps[0].t3[2];
		for(var j=0;j<=dj;j++){
			var x=(ps[2].t3[0]-px)*(j/dj)+px;
			var y=(ps[2].t3[1]-py)*(j/dj)+py;
			var z=(ps[2].t3[2]-pz)*(j/dj)+pz;
			var p=new Punto(ani,o.ti,o.tf,[x,y,z]);
			p.std=1;
			p.mapeo();
			if(p.std==2){
				x=Math.round(p.v2[0]/ani.zbdw);
				y=Math.round(p.v2[1]/ani.zbdh);
				if(x>=0&&y>=0&&x<=ani.zbwn&&y<=ani.zbhn){
					if(ani.zbuf[x+":"+y]){
						if(ani.zbuf[x+":"+y][0]!=o){
							if(ani.zbuf[x+":"+y][1]>p.v3[2]){
								if(ani.zbuf[x+":"+y][0].zbf+1>o.zbf){
									o.zbf=ani.zbuf[x+":"+y][0].zbf+1;
								}
								ani.zbuf[x+":"+y]=[o,p.v3[2]];
							}else{
								dls.push(ani.zbuf[x+":"+y][0]);
							}
						}
					}else{
						ani.zbuf[x+":"+y]=[o,p.v3[2]];
					}
				}
			}
		}
	}
	for(var i=0;i<dls.length;i++){
		if(dls[i].zbf<o.zbf+1){
			dls[i].zbf=o.zbf+1;
		}
	}
}
//(1-t)^2*A+2*t*(1-t)*B+t^2*C B->control ->Bezier cuadrática desde A hasta C
//Función para determinar de que lado del plano infinito se encuentra un punto.
function delantePlanoOrigen(a,b,c){
	return c[0]*(a[1]*b[2]-b[1]*a[2])+c[1]*(b[0]*a[2]-a[0]*b[2])+c[2]*(a[0]*b[1]-b[0]*a[1]);
}
function trasladarA(a,b,c,d){
	var x=[b[0]-a[0],b[1]-a[1],b[2]-a[2]];
	var y=[c[0]-a[0],c[1]-a[1],c[2]-a[2]];
	var z=[d[0]-a[0],d[1]-a[1],d[2]-a[2]];
	return [x,y,z];
}
function delantePlano(a,b,c,d){
	var x=[b[0]-a[0],b[1]-a[1],b[2]-a[2]];
	var y=[c[0]-a[0],c[1]-a[1],c[2]-a[2]];
	var z=[d[0]-a[0],d[1]-a[1],d[2]-a[2]];
	var r=delantePlanoOrigen(x,y,z);
	if(Math.abs(r)<0.0001){
		return 0;
	}
	return r;
}
//Función para determinar el punto medio entre dos vectores
function puntoMedio(a,b){
	var r=[];
	for(var i=0;i<a.length;i++){
		r[i]=(a[i]+b[i])/2;
	}
	return r;
}
//Interpolación lineal desde A hasta B
function interpolacionLineal(a,b,t){
	if(t<0||t>1){
		console.log('Error fatal');
		asdf.asdf
	}
	var r=[];
	for(var i=0;i<a.length;i++){
		r[i]=(b[i]-a[i])*t+a[i];
	}
	return r;
}
//Función para determinar si un punto se encuentra detrás de un triángulo:
//Retorna 0 si el punto no es ocultado por el triángulo, o si el punto hace parte del triángulo.
//Returna >0 si el punto está delante del triángulo.
//Returna <0 si el punto está detrás del triángulo.
function puntoDetrasTriangulo(a,b,c,d){
	//Algún punto al interior del triángulo.
	var mab=puntoMedio(a,b);
	var mbc=puntoMedio(b,c);
	var p=puntoMedio(mab,mbc);
	if(delantePlano([0,0,0],a,b,p)*delantePlano([0,0,0],a,b,d)<0){
		return 0;
	}
	if(delantePlano([0,0,0],b,c,p)*delantePlano([0,0,0],b,c,d)<0){
		return 0;
	}
	if(delantePlano([0,0,0],a,c,p)*delantePlano([0,0,0],a,c,d)<0){
		return 0;
	}
	return delantePlano(a,b,c,[0,0,0])*delantePlano(a,b,c,d);
}
//Función para determinar si un triángulo oculta a otro
//Un valor negativo indica que el segundo triángulo [d,e,f] está detrás del primero [a,b,c].
//Un valor positivo indica que el segundo triángulo [d,e,f] está delante del primero [a,b,c].
//Cero indica que los triángulos pertenecen al mismo plano o que no se ocultan entre ellos.
function delanteTriangulo(a,b,c,d,e,f){
	var vb=[d,e,f];
	for(var i=0;i<vb.length;i++){
		var r=puntoDetrasTriangulo(a,b,c,vb[i]);
		if(r!=0){
			/*console.log("A");
			console.log(a);
			console.log(b);
			console.log(c);
			console.log("B");
			console.log(d);
			console.log(e);
			console.log(f);
			console.log(r);*/
			return r;
		}
	}
	var va=[a,b,c];
	for(var i=0;i<va.length;i++){
		var r=puntoDetrasTriangulo(d,e,f,va[i]);
		if(r!=0){
			return -r;
		}
	}
	//Considerar la posibilidad de un intercepto
	var q1=va[0];
	for(var i=1;i<va.length;i++){
		var q2=va[i];
		for(var j=0;j<vb.length;j++){
			var k1=vb[j];
			var k2=vb[(j+1)%vb.length];
			var r1=delantePlano([0,0,0],k1,k2,q1);
			var r2=delantePlano([0,0,0],k1,k2,q2);
			if(r1*r2<0){
				var pm=puntoMedio(k1,k2);
				if(delantePlano([0,0,0],q1,k1,pm)*delantePlano([0,0,0],q1,k1,q2)>0&&delantePlano([0,0,0],q1,k2,pm)*delantePlano([0,0,0],q1,k2,q2)>0){
					var pl=interpolacionLineal(q1,q2,-r1/(r2-r1));
					return -delantePlano(d,e,f,[0,0,0])*delantePlano(d,e,f,pl);
				}
			}
		}
	}
	return 0;
}
//Retorna un arreglo de triángulos a partir de un polígono o ruta
function trias(o){
	var ps=[];
	for(var i=0;i<o.p.length;i++){
		if(o.p[i]&&o.p[i].t3){
			ps.push(o.p[i].t3);
		}
	}
	if(ps.length<3){
		alert('No es un polígono');
		return;
	}
	var rs=[];
	rs.push([ps[0],ps[1],ps[ps.length-1]]);
	var k=3;
	var ans=[ps[1],ps[ps.length-1]];
	var i=2;
	var j=2;
	while(true){
		rs.push([ps[i],ans[0],ans[1]]);
		ans=[ans[1],ps[i]];
		k++;
		i++;
		if(k>=ps.length){
			return rs;
		}
		rs.push([ps[ps.length-j],ans[0],ans[1]]);
		ans=[ans[1],ps[ps.length-j]];
		k++;
		j++;
		if(k>=ps.length){
			return rs;
		}
	}
	return rs;
}
//Función para determinar que elemento está detrás del otro. Se usará para ordenamiento
function ordenZ(a,b){
	if(!a.p||!b.p){
		return 0;
	}
	var ta=trias(a);
	var tb=trias(b);
	if(ta.length>tb.length){
		var x=ta;
		var y=tb;
	}else{
		var y=ta;
		var x=tb;
	}
	depurar=false;
	//if((a.nombre=='b'&&b.nombre=='c')||(a.nombre=='c'&&b.nombre=='b')){
	if(a.nombre=='c'||b.nombre=='c'){
		depurar=true;
		//ta.nombre=a.nombre;
		//tb.nombre=b.nombre;
		//print('('+a.nombre+','+b.nombre+')');
		//print('('+a.aid+','+b.aid+')');
	}
	var inc=1;
	for(var k=0;k<2;k++){
		for(var i=0;i<x.length;i++){
			for(var j=0;j<y.length;j++){
				var s=0;
				var e=true;
				for(var d=0;d<3;d++){
					var r=delantePlano(x[i][0],x[i][1],x[i][2],y[j][d]);
					if(s==0){
						s=Math.sign(r);
					}else{
						if(Math.abs(r)>=0.01){
							if(s*Math.sign(r)<0){
								e=false;
								break;
							}
						}
					}
				}
				if(e){
					var r=delantePlano(x[i][0],x[i][1],x[i][2],[0,0,0]);
					if(depurar){
						//print('('+a.nombre+','+b.nombre+')'+x.nombre+':'+y.nombre+'|'+inc+'->:'+(s*Math.sign(r)*inc));
						//print(a.aid+":"+b.aid+":"+s*Math.sign(r)*inc);
					}
					return s*Math.sign(r)*inc;
				}
			}
		}
		var t=x;
		var x=y;
		var y=t;
		inc=-1;
	}
	return 0;
}
//
function delantePoligono(a,b){
	if(!a.p||!b.p){
		return 0;
	}
	var ta=trias(a);
	var tb=trias(b);
	for(var i=0;i<ta.length;i++){
		for(var j=0;j<tb.length;j++){
			var r=delanteTriangulo(ta[i][0],ta[i][1],ta[i][2],tb[j][0],tb[j][1],tb[j][2]);
			if(Math.abs(r)>0.000001){
				return r;
			}
		}
	}
	return 0;
}
function actualizarSiguientes(a){
	for(var i=0;i<a.zbfs.length;i++){
		var s=a.zbfs[i];
		if(s.zbf<=a.zbf){
			s.zbf=a.zbf+1;
			actualizarSiguientes(s);
		}
	}
}
function ordenZ02(a){
	var procesados=[];
	for(var i=0;i<a.length;i++){
		if(!a[i].p){
			continue;
		}
		a[i].zbf=0;
		a[i].zbfs=[];
		for(var j=0;j<procesados.length;j++){
			var r=delantePoligono(a[i],procesados[j]);
			if(r<0){
				procesados[j].zbfs.push(a[i]);
				if(procesados[j].zbf+1>a[i].zbf){
					a[i].zbf=procesados[j].zbf+1;
				}
			}else{
				if(r>0){
					a[i].zbfs.push(procesados[j]);
				}
			}
		}
		actualizarSiguientes(a[i]);
		procesados.push(a[i]);
	}
	for(var i=0;i<procesados.length;i++){
		console.log(procesados[i].id+":"+procesados[i].zbf);
	}
}
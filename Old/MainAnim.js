function MainAnim(){
//Agregar alg�n elemento
	this.agregar=function(e){
		if(!e)return 0;
		if(this.e[e.t]){
			this.e[e.t].push(e);
			e.id=this.e[e.t].length;
		}else{
			this.e[e.t]=[];
			e.id=1;
			this.e[e.t].push(e);
		}
		if(e.tf>this.d)this.d=e.tf;
		return 1;
	}
//Calcula la animaci�n en el tiempo t, ver el m�todo interpolar de la clase anim
	this.calcular=function(t){
		if(t>=this.d)return 1;
		var niveles={};
		var lm=0;
		for(var i=0;i<this.e[2].length;i++){
			var tan=this.e[2][i];
			if(tan.interpolar(t)){
				if(tan.l>lm)lm=tan.l;
				if(!niveles[tan.l]){
					niveles[tan.l]=[];
				}
				niveles[tan.l].push(tan);
			}
		}
		var res="";
		for(var i=0;i<=lm;i++){
			if(niveles[i]){
				var tc=niveles[i];
				for(var j=0;j<tc.length;j++){
					var tb=tc[j].b;
					tb.posicionar();
					for(var k=0;k<tb.g.length;k++){
						tb.g[k].calcular();
						this.g.push(tb.g[k]);
					}
				}
			}
		}
	}
//Exportar el archivo a un gr�fico
	this.dibujar=function(){
		return graficador.dMainAnim(this);
  }
//Reset calculos
	this.reset=function(){
		for(var i=0;i<this.all.length;i++){
			this.all[i].std=0;
		}
		this.g=[];
	}
	this.e=[];
	this.g=[];
	this.zmin=0.1;
	this.visualx=0.5;
	this.visualy=0.375;
	this.resx=800;
	this.resy=600;
	this.d=0;
	this.e[1]=[];
	this.all=[];
	new Base(this,0,0,[0,0,0],[0,0,0]);
	this.rad=3.14159265358979/180;
}
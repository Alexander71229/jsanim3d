//Base(ani,ti,tf,[a1],[p1])
//Anim(ani,d,[a2],[p2])
//Color(ani,[c])
//Punto(ani,ti,tf,[v3],[c])
//Linea(ani,ti,tf,[p1],[p2],[c])
//Poligono(ani,ti,tf,[p],[c])
//Orto(ani,ti,tf,x,y,z,[c])
m=new MainAnim();
b=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[0,-3,30]);
//m.e[1][0].enlazarBase(b);

b.enlazarBase(m.e[1][0]);

b2=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[0,0,5]);
b2.enlazarBase(b);

c=new Orto(m,0,30,1,1,5,[0,0,0]);
c2=new Orto(m,0,30,1,1,2,[0,0,0]);
b.agregar(c);
b2.agregar(c2);

a=new Anim(m,10,[m.rad*180,m.rad*45,-m.rad*90],[0,-3,30]);
b.agregarAnim(a);
a2=new Anim(m,10,[m.rad*360,m.rad*0,m.rad*0],[0,-3,30]);
a.agregarS(a2);


a3=new Anim(m,10,[m.rad*180,m.rad*0,m.rad*0],[0,0,5]);
a4=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[0,0,5]);
b2.agregarAnim(a3);
a3.agregarS(a4);

function dibujar(i){
	m.calcular(i);
	m.dibujar();
	m.reset();
	if(i<m.d){
		setTimeout(function(){dibujar(i+.03)},1);
	}
}
dibujar(0);
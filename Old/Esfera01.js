try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[0,0,150]);
	b.enlazarBase(m.e[1][0]);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[97,109,107]);
	colores[2]=new Color(m,[185,178,126]);
	colores[3]=new Color(m,[95,83,62]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[0,235,245]);
	colores[6]=new Color(m,[235,235,245]);
	var r=10;
	var h=-10;
	var tx=Math.sqrt(2)-1;
	var tg=Math.sqrt(2)/2;
	//cruz
	c=new Linea(m,0,30,new Punto(m,0,30,[-1,0,0]),new Punto(m,0,30,[1,0,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,-1,0]),new Punto(m,0,30,[0,1,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,0,-1]),new Punto(m,0,30,[0,0,1]),false);
	b.agregar(c);	
	//segmento01
	var puntos=[];
	puntos.push(new Punto(m,0,300,[r,0,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[r,h,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r,h,tx*r],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[r*tg,h,r*tg],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[r*tg,0,r*tg],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r,0,tx*r],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[r,0,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[0],colores[6]);
	b.agregar(c);

	a=new Anim(m,10,[-m.rad*0,m.rad*0,m.rad*0],[0,0,150]);
	b.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
function Anim(ani,d,a2,p2){
  if(window.ais){
    ais++;
    this.aid=ais;
  }else{
    window.ais=1;
    this.aid=1;
  }
  window["elemento"+this.aid]=this;
  this.tipox="Anim";
//Estable los �ngulos y la posici�n de la base en el tiempo t
	this.interpolar=function(t){
    if(t<this.ti)return 0;
    if(t>=this.tf)return 0;
    if(t<this.ti+this.d){
			var tt=(t-this.ti)/this.d;
      for(i=0;i<3;i++){
				this.b.a[i]=this.a2[i]*tt+this.a1[i]*(1-tt);
				this.b.p[i]=this.p2[i]*tt+this.p1[i]*(1-tt);
      }
    }else{
			this.b.a=this.a2;
			this.b.p=this.p2;
    }
		this.b.calcular();
		return 1;
  }
//Agregar animaci�n siguiente, suponiendo que esta Animaci�n est� enlazada a una cadena con base
	this.agregarS=function(a){
    if(this.sg){
			this.sg.an=a;
			this.sg.a1=a.a2;
			this.sg.p1=a.p2;
    }
		a.sg=this.sg;
		this.sg=a;
		a.an=this;
		a.a1=this.a2;
		a.p1=this.p2;
		a.b=this.b;
		a.l=a.b.l;
		this.sincro();
  }
//Sincronizar animaci�n
	this.sincro=function(){
  	var sg=this.b.s;
    while(sg){
			sg.tf=sg.ti+sg.d;
			//resultado.value=sg.tf;
      //if(this.b.tf<sg.tf)this.b.tf=sg.tf;
      if(sg.sg){
      	sg.sg.ti=sg.tf;
      }else{
				//sg.tf=this.b.tf;
				break;
      }
	    sg=sg.sg;
    }
  }
	this.ani=ani;          //Animaci�n principal
	this.t=2;            //Tipo de objeto
	this.id=0;           //Identificador
	this.l=0;            //Nivel
	this.ti=0;          //tiempo inicial
	this.tf=0;          //tiempo final
	this.d=d||0;        //Duraci�n de la base
	this.an=0;           //Animaci�n anterior
	this.sg=0;           //Animaci�n siguiente
	this.b=null;            //Base asociada
	this.a1;          //Angulos iniciales
	this.a2=a2||[];          //Angulos finales
	this.p1;          //Posici�n inicial
	this.p2=p2||[];          //Posici�n final
	this.std=0;          //Estado de la base, usado para los c�lculos
	this.ani.agregar(this);
}
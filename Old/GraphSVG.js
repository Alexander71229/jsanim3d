function GraphSVG(){
	this.dMainAnim=function(mainAnim){
		//SVG
		var res='<?xml version="1.0" standalone="no"?>';
		res=res+'<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
		res=res+"<svg width='"+mainAnim.resx+"px' height='"+mainAnim.resy+"px' viewBox='0 0 "+mainAnim.resx+" "+mainAnim.resy+"' xmlns='http://www.w3.org/2000/svg'>";
    for(var i=0;i<mainAnim.g.length;i++){
    	res=res+mainAnim.g[i].dibujar();
    }
		res=res+"</svg>";
		return res;
	}
	this.dPunto=function(punto){
		if(punto.std!=2)return;
		return "<circle cx='"+punto.v2[0]+"' cy='"+punto.v2[1]+"' r='1' fill='"+punto.c.rep1+"' stroke='none'/>";
  }
	this.dLinea=function(linea){
    if(linea.std!=2)return "";
		//return "<line x1='"+linea.p1.v2[0]+"' y1='"+linea.p1.v2[1]+"' x2='"+linea.p2.v2[0]+"' y2='"+linea.p2.v2[1]+"' stroke='"+linea.c.rep1+"'/>";
		return "<line x1='"+linea.p1.v2[0]+"' y1='"+linea.p1.v2[1]+"' x2='"+linea.p2.v2[0]+"' y2='"+linea.p2.v2[1]+"' stroke='#000000'/>";
  }
}
graficador=new GraphSVG();

try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[0,0,300]);
	b.enlazarBase(m.e[1][0]);
	//c=new Orto(m,0,30,20,10,10,[0,0,0]);
	puntos=[];
	puntos.push(new Punto(m,0,30,[10,0,-7],[0,0,0]));
	puntos.push(new Punto(m,0,30,[7,-2,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[1,0,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[1.3,1,-1],[0,0,0]));
	puntos.push(new Punto(m,0,30,[5.4,1,-4.5],[0,0,0]));
	puntos.push(new Punto(m,0,30,[9.5,0,-4.5],[0,0,0]));
	puntos.push(new Punto(m,0,30,[5.4,-0.5,-4.5],[0,0,0]));
	
	c=new Linea(m,0,30,puntos[0],puntos[1],false);
	b.agregar(c);
	c=new Linea(m,0,30,puntos[1],puntos[2],false);
	b.agregar(c);
	c=new Linea(m,0,30,puntos[3],puntos[4],false);
	b.agregar(c);
	c=new Linea(m,0,30,puntos[4],puntos[5],false);
	b.agregar(c);
	c=new Linea(m,0,30,puntos[5],puntos[6],false);
	b.agregar(c);
	c=new Linea(m,0,30,puntos[6],puntos[3],false);
	b.agregar(c);

	//a=new Anim(m,10,[m.rad*180,m.rad*45,-m.rad*90],[0,-3,30]);
	a=new Anim(m,10,[-m.rad*90,m.rad*0,-m.rad*0],[0,0,300]);
	b.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d){
			setTimeout(function(){dibujar(i+.03)},1);
		}
	}
	dibujar(0);
	//alert('ok');
}catch(e){
	alert(e);
}
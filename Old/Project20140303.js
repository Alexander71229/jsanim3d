try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[10,10,150]);
	b.enlazarBase(m.e[1][0]);
	var puntos=[];
	puntos.push(new Punto(m,0,30,[-3,-3,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-3,3,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[3,3,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[3,-3,-3],[0,0,0]));
	c=new Poligono(m,0,3,puntos,new Color(this.ani,[255,0,0]),new Color(this.ani,[0,0,255]));
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[-3,-3,3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-3,3,3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[3,3,3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[3,-3,3],[0,0,0]));
	c=new Poligono(m,0,3,puntos,new Color(this.ani,[0,0,0]),new Color(this.ani,[255,255,0]));
	b.agregar(c);

	c=new Linea(m,0,30,new Punto(m,0,30,[-1,0,0]),new Punto(m,0,30,[1,0,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,-1,0]),new Punto(m,0,30,[0,1,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,0,-1]),new Punto(m,0,30,[0,0,1]),false);
	b.agregar(c);

	c=new C2Bezier(m,0,30,new Punto(m,0,30,[3,-3,-3]),new Punto(m,0,30,[0,-6,0]),new Punto(m,0,30,[-3,-3,-3]),false);
	b.agregar(c);
	c=new C2Bezier(m,0,30,new Punto(m,0,30,[3,-3,3]),new Punto(m,0,30,[0,-6,0]),new Punto(m,0,30,[-3,-3,3]),false);
	b.agregar(c);
	c=new C2Bezier(m,0,30,new Punto(m,0,30,[-3,-3,3]),new Punto(m,0,30,[0,-6,0]),new Punto(m,0,30,[-3,-3,-3]),false);
	b.agregar(c);
	c=new C2Bezier(m,0,30,new Punto(m,0,30,[3,-3,3]),new Punto(m,0,30,[0,-6,0]),new Punto(m,0,30,[3,-3,-3]),false);
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[-3,3,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[0,6,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[3,3,-3],[0,0,0]));
	c=new CPath(m,0,3,puntos,new Color(this.ani,[255,0,0]),new Color(this.ani,[0,255,255]));
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[-3,3,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[0,6,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-3,3,3],[0,0,0]));
	c=new CPath(m,0,3,puntos,new Color(this.ani,[255,0,0]),new Color(this.ani,[0,255,255]));
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[3,3,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[0,6,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[3,3,3],[0,0,0]));
	c=new CPath(m,0,3,puntos,new Color(this.ani,[255,0,0]),new Color(this.ani,[0,255,255]));
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[-3,3,3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[0,6,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[3,3,3],[0,0,0]));
	c=new CPath(m,0,3,puntos,new Color(this.ani,[255,0,0]),new Color(this.ani,[0,255,255]));
	b.agregar(c);
	
	//a=new Anim(m,10,[m.rad*180,m.rad*45,-m.rad*90],[0,-3,30]);
	a=new Anim(m,10,[-m.rad*180,m.rad*0,-m.rad*0],[0,0,150]);
	b.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
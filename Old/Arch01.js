try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*45,m.rad*0,m.rad*0],[0,0,350]);
	b.enlazarBase(m.e[1][0]);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[13,44,-3],[0,0,0]));
	//puntos.push(null);
	puntos.push(new Punto(m,0,30,[15,42,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[18,41,-3],[0,0,0]));
	//puntos.push(null);
	puntos.push(new Punto(m,0,30,[22,40,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[25,40.1,-3],[0,0,0]));
	//puntos.push(null);
	puntos.push(new Punto(m,0,30,[29,39.8,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[32,41.5,-3],[0,0,0]));
	//puntos.push(null);
	puntos.push(new Punto(m,0,30,[34,42,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[35.8,44.6,-2],[0,0,0]));
	puntos.push(null);
	puntos.push(new Punto(m,0,30,[30,50.2,-3],[0,0,0]));
	puntos.push(null);
	puntos.push(new Punto(m,0,30,[31.7,44.3,-3],[0,0,0]));
	//puntos.push(null);
	puntos.push(new Punto(m,0,30,[29.5,42,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[27.4,41.6,-3],[0,0,0]));
	//puntos.push(null);
	puntos.push(new Punto(m,0,30,[24.4,40.8,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[21,41.7,-3],[0,0,0]));
	//puntos.push(null);
	puntos.push(new Punto(m,0,30,[16,43,-3],[0,0,0]));
	puntos.push(new Punto(m,0,30,[12.6,45.5,-3],[0,0,0]));
	c=new CPath(m,0,3,puntos,new Color(this.ani,[0,0,0]),new Color(this.ani,[19,31,31]));
	//c=new Poligono(m,0,3,puntos,new Color(this.ani,[255,0,0]),new Color(this.ani,[0,255,255]));
	b.agregar(c);
	
	//a=new Anim(m,10,[m.rad*180,m.rad*45,-m.rad*90],[0,-3,30]);
	a=new Anim(m,10,[m.rad*45,m.rad*0,-m.rad*0],[0,0,350]);
	b.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
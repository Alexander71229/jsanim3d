try{
	m=new MainAnim();
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[0,0,140]);
	b1.enlazarBase(m.e[1][0]);
	b2=new Base(m,0,300,[m.rad*0,m.rad*0,-m.rad*0],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	colores[6]=new Color(m,[255,255,255]);
	colores[7]=new Color(m,[0,0,255]);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[-10,10,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[30,10,80],[0,0,0]));
	puntos.push(new Punto(m,0,30,[30,20,80],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-10,20,0],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[7],colores[7]);
	c.nombre='b';
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[-20,10,30],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-10,10,30],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-10,20,30],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-20,20,30],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[1],colores[1]);
	c.nombre='a';
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[20,10,30],[0,0,0]));
	puntos.push(new Punto(m,0,30,[30,10,30],[0,0,0]));
	puntos.push(new Punto(m,0,30,[30,20,30],[0,0,0]));
	puntos.push(new Punto(m,0,30,[20,20,30],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[1],colores[1]);
	c.nombre='c';
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[20,10,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[30,10,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[30,20,0],[0,0,0]));
	puntos.push(new Punto(m,0,30,[20,20,0],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[2],colores[2]);
	c.nombre='d';
	b.agregar(c);

	var puntos=[];
	puntos.push(new Punto(m,0,30,[-20,10,-10],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-8,10,-10],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-8,20,-10],[0,0,0]));
	puntos.push(new Punto(m,0,30,[-20,20,-10],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[2],colores[2]);
	c.nombre='e';
	b.agregar(c);

	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[0,0,140]);
	b1.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,-m.rad*0,-m.rad*0],[0,0,0]);
	b2.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
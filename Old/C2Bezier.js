function C2Bezier(ani,ti,tf,p1,pc,p2,c){
  if(window.ais){
    ais++;
    this.aid=ais;
  }else{
    window.ais=1;
    this.aid=1;
  }
  window["elemento"+this.aid]=this;
  this.tipox="C2Bezier";

	this.agregar=function(b){
		this.p1.agregar(b);
		this.pc.agregar(b);
		this.p2.agregar(b);
  }
//Ajustar un segmento de recta con otro que sea completamente visible pero que sea representativo
//Temporalmente incompleta, falta cortar x y y
	this.segmentar=function(){
		var l=this;
    if(l.std==0){
    	var t;
      if(l.p1.t3[2]<this.ani.zmin&&l.p2.t3[2]<this.ani.zmin){
				//ningun punto es visible: la linea no se debe dibujar
				return 3;
      }else{
      }
			this.std=1;
			this.p1.std=1;
			this.p2.std=1;
    }
  }
	this.calcular=function(){
		this.p1.calcular();
		this.pc.calcular();
		this.p2.calcular();
		this.zO=this.gZ();
	  this.segmentar();
    if(this.std!=1)return 0;
		this.p1.mapeo();
		this.pc.mapeo();
		this.p2.mapeo();
		this.std=2;
		return 1;
  }
//Exportaci�n a gr�fico
	this.dibujar=function(){
		return graficador.dC2Bezier(this);
  }
	this.gZ=function(){
		return (this.p1.gD()+this.p2.gD()+this.pc.gD())/3;
	}
	this.t=8; //tipo de elemento
	this.ani=ani;
	this.std=0; //Estado de los c�lculos
	this.ti=ti||0; //tiempo inicial
	this.tf=tf||0; //Tiempo final
	this.d=0; //Duraci�n
	this.b=null; //id base
	this.id=0; //Identificador �nico
	this.p1=p1; //Punto1
	this.pc=pc; //Punto de control
	this.p2=p2; //Punto2
	this.c=c||new Color(this.ani,[0,0,0]); //Color
	this.ani.all.push(this);
}
function Base(ani,ti,tf,a1,p1){
  if(window.ais){
    ais++;
    this.aid=ais;
  }else{
    window.ais=1;
    this.aid=1;
  }
  window["elemento"+this.aid]=this;
  this.tipox="Base";

//Agregar elemento gr�fico a la base
	this.agregar=function(v){
		v.agregar(this);
		v.b=this;
		v.l=this.l+1;
		this.g.push(v);
	}
//Calcular la matriz de rotaci�n seg�n los �ngulos actuales
	this.calcular=function(){
		//Vi  
		this.m[0][0]=Math.cos(this.a[0])*Math.cos(this.a[2])+Math.sin(this.a[0])*Math.sin(this.a[1])*Math.sin(this.a[2]);
		this.m[0][1]=Math.cos(this.a[1])*Math.sin(this.a[2]);
		this.m[0][2]=Math.sin(this.a[0])*Math.cos(this.a[2])-Math.cos(this.a[0])*Math.sin(this.a[1])*Math.sin(this.a[2]);

		//Vj
		this.m[1][0]=-Math.cos(this.a[0])*Math.sin(this.a[2])+Math.sin(this.a[0])*Math.sin(this.a[1])*Math.cos(this.a[2]);
		this.m[1][1]=Math.cos(this.a[1])*Math.cos(this.a[2]);
		this.m[1][2]=-Math.sin(this.a[0])*Math.sin(this.a[2])-Math.cos(this.a[0])*Math.sin(this.a[1])*Math.cos(this.a[2]);

		//Vk
		this.m[2][0]=-Math.sin(this.a[0])*Math.cos(this.a[1]);
		this.m[2][1]=Math.sin(this.a[1]);
		this.m[2][2]=Math.cos(this.a[0])*Math.cos(this.a[1]);
		this.std = 1
  }
//Calcula la posici�n absoluta de una base de acuerdo a su elemento padre
	this.posicionar=function(){
    if(!this.b||(this.b.id==1)){
			this.std=2;
			return 1;
    }else{
      if(this.b.std!=2)return 2;
			this.m=mxm(this.b.m,this.m);
			this.p=mxp(this.b.m,this.p);
			for(var i=0;i<3;i++){
				this.p[i]+=this.b.p[i];
			}
			this.std=2;
    }
  }
//Multiplicar un punto
	this.mul=function(p){
  	p.t3=mxp(this.m,p.v3);
  }
//Agregar esta base a una base padre
	this.enlazarBase=function(b){
		this.b=b;
		this.l=b.l+1;
  }
//Agregar animaci�n inicial
	this.agregarAnim=function(a){
    if(this.s){
			this.s.an=a;
			this.s.a1=a.a2;
			this.s.p1=a.p2;
    }
		a.sg=this.s;
		this.s=a;
		a.an=null;
		a.a1=this.a1;
		a.p1=this.p1;
		a.b=this;
		a.l=this.l;
		this.s.ti=this.ti;
		a.sincro();
  }
	this.g=[];
	this.ani=ani; //Referencia al MainAnim
	this.t=1;  //Tipo de elemento
	this.id=0; //identificador �nico
	this.ti=ti||0; //Tiempo inicial
	this.tf=tf||0; //Tiempo final
	this.a1=a1||[0,0,0]; //Angulos iniciales
	this.p1=p1||[0,0,0]; //Posici�n inicial
	this.l=0; //Nivel de jerarqu�a de la base
	this.s=0; //Primer elemento Anim 
	this.b=null; //Base padre
	this.a=[]; //�ngulos temporales
	this.p=[]; //Posici�n temporal para alg�n tiempo
	this.m=[];           //Matriz de rotaci�n seg�n los angulos a
  for(var i=0;i<3;i++){
  	this.m[i]=[];
  }
	this.ani.agregar(this);
	this.agregarAnim(new Anim(this.ani,0,this.a1,this.p1));
}
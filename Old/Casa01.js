try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[0,0,150]);
	b.enlazarBase(m.e[1][0]);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[97,109,107]);
	colores[2]=new Color(m,[185,178,126]);
	colores[3]=new Color(m,[95,83,62]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	//cubo
	var puntos=[];
	puntos.push(new Punto(m,0,300,[30,20,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,20,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,10,40],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[1]);
	b.agregar(c);
	
	//a=new Anim(m,10,[m.rad*180,m.rad*45,-m.rad*90],[0,-3,30]);
	a=new Anim(m,10,[m.rad*90,m.rad*0,m.rad*0],[30,0,150]);
	b.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
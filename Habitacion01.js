try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[0,0,150]);
	b.enlazarBase(m.e[1][0]);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	//cubo
	var puntos=[];
	puntos.push(new Punto(m,0,300,[30,20,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,20,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,10,40],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[1]);
	b.agregar(c);
	var puntos=[];
	puntos.push(new Punto(m,0,300,[30,10,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,10,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,40],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[1]);
	b.agregar(c);
	var puntos=[];
	puntos.push(new Punto(m,0,300,[20,10,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,20,40],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[1]);
	b.agregar(c);
	//Pared 1
	var puntos=[];
	puntos.push(new Punto(m,0,300,[30,-20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,-20,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Pared inferior
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-20,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-20,20,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	b.agregar(c);
	//Separador
	var puntos=[];
	puntos.push(new Punto(m,0,300,[0,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[5,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[5,-15,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	b.agregar(c);
	//Lateral 1
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-20,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-15,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-15,-15,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-20,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	b.agregar(c);
	//Lateral 2
	var puntos=[];
	puntos.push(new Punto(m,0,300,[20,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,-15,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	b.agregar(c);
	//Superior
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-20,-20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,-20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,-15,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-20,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	b.agregar(c);
	//Marco inferior 1
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-15,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-15,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,5,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Marco inferior 2
	var puntos=[];
	puntos.push(new Punto(m,0,300,[5,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[5,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,5,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Marco superior 1
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-15,-15,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-15,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Marco superior 2
	var puntos=[];
	puntos.push(new Punto(m,0,300,[5,-15,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[5,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Marco lateral 1
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-15,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-15,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-15,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-15,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Marco lateral 2
	var puntos=[];
	puntos.push(new Punto(m,0,300,[0,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[0,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Marco lateral 3
	var puntos=[];
	puntos.push(new Punto(m,0,300,[5,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[5,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[5,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[5,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Marco lateral 4
	var puntos=[];
	puntos.push(new Punto(m,0,300,[20,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Ventana 1
	var puntos=[];
	puntos.push(new Punto(m,0,300,[20,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,5,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,-15,53],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,-15,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[0],colores[3]);
	b.agregar(c);
	//Suelo
	var k=0;
	for(var x=-20;x<30;x=x+10){
		for(var z=0;z<50;z=z+10){
			k++;
			var puntos=[];
			puntos.push(new Punto(m,0,300,[x,20,z+10],[0,0,0]));
			puntos.push(new Punto(m,0,300,[x,20,z],[0,0,0]));
			puntos.push(new Punto(m,0,300,[x+10,20,z],[0,0,0]));
			puntos.push(new Punto(m,0,300,[x+10,20,z+10],[0,0,0]));
			if(k%2==1){
				c=new Poligono(m,0,3,puntos,colores[4],colores[5]);
			}else{
				c=new Poligono(m,0,3,puntos,colores[5],colores[4]);
			}
			b.agregar(c);
		}
	}
	
	//a=new Anim(m,10,[m.rad*180,m.rad*45,-m.rad*90],[0,-3,30]);
	a=new Anim(m,10,[m.rad*90,m.rad*0,m.rad*0],[30,0,150]);
	b.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
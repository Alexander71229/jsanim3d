function Poligono(ani,ti,tf,p,c,cr){
	//c=AGC(ani);
	//cr=AGC(ani);
  if(window.ais){
    ais++;
    this.aid=ais;
  }else{
    window.ais=1;
    this.aid=1;
  }
  window["elemento"+this.aid]=this;
  this.tipox="Poligono";

	this.agregar=function(b){
    //for(var i=0;i<this.ln.length;i++){
    	//this.ln[i].agregar(b);
    //}
		for(var i=0;i<this.p.length;i++){
			this.p[i].agregar(b);
		}
  }
	this.calcular=function(){
    //for(var i=0;i<this.ln.length;i++){
    	//this.ln[i].calcular();
    //}
		this.std=2;
		for(var i=0;i<this.p.length;i++){
			this.p[i].calcular();
			this.p[i].std=1;
			this.p[i].mapeo();
			if(this.p[i].std==3){
				this.std=3;
			}
		}
		//this.zO=this.gZ();
		//fzbufp(this.ani,this);
  }
//Exportaci�n a gr�fico
	this.dibujar=function(){
  	var res="";
		if(this.std==3){
			return res;
		}
    //for(var i=0;i<this.ln.length;i++){
    	//res=res+this.ln[i].dibujar();
    //}
		res=graficador.dPoligono(this);
  	return res;
  }
	this.gZ=function(){
		var tx=0;
		for(var i=0;i<this.p.length;i++){
			tx=tx+this.p[i].gD();
		}
		return tx/this.p.length;
	}
	this.toString=function(){
		return this.aid;
	}
	this.t=7; //tipo de elemento
	this.ani=ani;
	this.std=0; //Estado de los c�lculos
	this.ti=ti||0; //tiempo inicial
	this.tf=tf||0; //Tiempo final
	this.d=0; //Duraci�n
	this.b=null; //base
	this.id=0; //Identificador �nico
	if(!window.poligonoid){
		window.poligonoid=1;
	}else{
		window.poligonoid++;
	}
	this.id=poligonoid;
	this.p=p; //Puntos del pol�gono
	this.c=c||new Color(this.ani,[0,0,0]); //Color
	this.cr=cr||new Color(this.ani,[255,255,255]);
	this.ln=[]; //Array de lineas;
  for(var i=0;i<this.p.length-1;i++){
  	//this.ln.push(new Linea(this.ani,this.ti,this.tf,new Punto(this.ani,this.ti,this.tf,this.p[i]),new Punto(this.ani,this.ti,this.tf,this.p[i+1]),this.c));
  }
	//this.ln.push(new Linea(this.ani,this.ti,this.tf,new Punto(this.ani,this.ti,this.tf,this.p[this.p.length-1]),new Punto(this.ani,this.ti,this.tf,this.p[0]),this.c));
	this.ani.all.push(this);
}
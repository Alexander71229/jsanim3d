function GCanvas(canvas){
	this.canvas=canvas;
	try{
		this.canvas=this.canvas||document.getElementsByTagName('canvas')[0];
	}catch(e){}
	this.canvas=this.canvas;
	if(!this.canvas){
		this.canvas=document.createElement('canvas');
		document.body.appendChild(this.canvas);
	}
	this.dMainAnim=function(mainAnim){
		this.canvas.width=mainAnim.resx;
		this.canvas.height=mainAnim.resy;
		this.ctx=this.canvas.getContext('2d');
		var gx=[];
		if(!mainAnim.profundidad){
			for(var i=0;i<mainAnim.g.length;i++){
				gx.push(mainAnim.g[i]);
			}
			//Warning
			ordenZ02(gx); //Actual algoritmo de profundidad
			gx.sort(function(a,b){
				return a.zbf-b.zbf;
			});
		}else{
			for(var i=0;i<mainAnim.g.length;i++){
				gx[mainAnim.profundidad[mainAnim.g[i].aid]]=mainAnim.g[i];
			}
		}
		this.ctx.fillStyle='#FFFFFF';
		this.ctx.fillRect(0,0,4000,3000);
		for(var i=0;i<gx.length;i++){
			//print("hashProfundidad["+gx[i].aid+"]="+i+";");
			gx[i].dibujar();
		}
	}
	this.dPunto=function(punto){
		if(punto.std!=2)return;
		this.ctx.fillRect(punto.v2[0],punto.v2[1],punto.v2[0]+1,punto.v2[1]+1);
	}
	this.dLinea=function(linea){
    if(linea.std!=2)return;
		//this.ctx.strokeStyle="#000000";
		this.ctx.strokeStyle=linea.c;
    this.ctx.beginPath();
    this.ctx.moveTo(linea.p1.v2[0],linea.p1.v2[1]);
    this.ctx.lineTo(linea.p2.v2[0],linea.p2.v2[1]);
    this.ctx.stroke();
  }
	this.dPoligono=function(poligono){
		if(poligono.std!=2)return;
		this.ctx.fillStyle=poligono.cr.rep1();
		this.ctx.strokeStyle=poligono.c.rep1();
    this.ctx.beginPath();
		this.ctx.moveTo(poligono.p[0].v2[0],poligono.p[0].v2[1]);
		for(var i=1;i<poligono.p.length;i++){
			this.ctx.lineTo(poligono.p[i].v2[0],poligono.p[i].v2[1]);
		}
		//this.ctx.lineTo(poligono.p[i].v2[0],poligono.p[i].v2[1]);
		this.ctx.closePath();
    this.ctx.stroke();
		this.ctx.fill();
		this.dTextura(poligono);
	}
	this.dC2Bezier=function(bezier){
		if(bezier.std!=2)return;
		this.ctx.strokeStyle="#000000";
    this.ctx.beginPath();
    this.ctx.moveTo(bezier.p1.v2[0],bezier.p1.v2[1]);
    this.ctx.quadraticCurveTo(bezier.pc.v2[0],bezier.pc.v2[1],bezier.p2.v2[0],bezier.p2.v2[1]);
    this.ctx.stroke();
	}
	this.dCPath=function(cpath){ //(1-t)^2*A+2*t*(1-t)*B+t^2*C B->control ->Bezier cuadrática
		if(cpath.std!=2)return;
		this.ctx.fillStyle=cpath.cr.rep1();
		this.ctx.strokeStyle=cpath.c.rep1();
    this.ctx.beginPath();
		this.ctx.moveTo(cpath.p[0].v2[0],cpath.p[0].v2[1]);
		for(var i=1;i+1<cpath.p.length;i=i+2){
			if(cpath.p[i]){
				this.ctx.quadraticCurveTo(cpath.p[i].v2[0],cpath.p[i].v2[1],cpath.p[i+1].v2[0],cpath.p[i+1].v2[1]);
			}else{
				this.ctx.lineTo(cpath.p[i+1].v2[0],cpath.p[i+1].v2[1]);
			}
		}
		this.ctx.closePath();
    this.ctx.stroke();
		this.ctx.fill();
	}
	this.dTextura=function(poligono){
		if(poligono.p.length==4&&poligono.textura){
			var textura=poligono.textura;
			//try{
				for(var i=0;i<textura.elementos.length;i++){
					var ruta=textura.elementos[i];
					this.ctx.fillStyle=ruta.c1.rep1();
					this.ctx.strokeStyle=ruta.c2.rep1();
					this.ctx.beginPath();
					for(var j=0;j<ruta.elementos.length;j++){
						var elemento=ruta.elementos[j];
						if(elemento instanceof Mover2D){
							var p=conversion2D(elemento.x,elemento.y,textura,poligono.p);
							this.ctx.moveTo(p[0],p[1]);
						}
						if(elemento instanceof Linea2D){
							var p=conversion2D(elemento.x,elemento.y,textura,poligono.p);
							this.ctx.lineTo(p[0],p[1]);
						}
						if(elemento instanceof Curva2D){
							var c=conversion2D(elemento.cx,elemento.cy,textura,poligono.p);
							var p=conversion2D(elemento.x,elemento.y,textura,poligono.p);
							this.ctx.quadraticCurveTo(c[0],c[1],p[0],p[1]);
						}
					}
					this.ctx.closePath();
					this.ctx.stroke();
					this.ctx.fill();
				}
			//}catch(e){
			//}
		}
	}
}
graficador=new GCanvas();
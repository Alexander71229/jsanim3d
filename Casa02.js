try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*20,m.rad*0,m.rad*0],[0,-1,50]);
	b.enlazarBase(m.e[1][0]);
	var colores=[];
	colores[0]=new Color(m,[255,255,255]);
	colores[1]=new Color(m,[240,225,240]);
	colores[2]=new Color(m,[255,240,225]);
	colores[3]=new Color(m,[225,220,225]);
	colores[4]=new Color(m,[240,225,255]);
	colores[5]=new Color(m,[240,225,255]);
	//Puerta
	lamina(m,b,0,30,null,null,null,[-.5,-1,5],[.5,-1,5],[.5,1,5],[-.5,1,5],1,1,null);
	var parametros=[];
	//Vigas de la entrada
	parametros.push(new ParametrosLamina(null,null,null,1,1,null,null));
	parametros.push(new ParametrosLamina(null,null,null,1,1,null,null));
	parametros.push(new ParametrosLamina(null,null,null,1,1,null,null));
	parametros.push(new ParametrosLamina(null,null,null,1,1,null,null));
	parametros.push(new ParametrosLamina(null,null,null,1,1,null,null));
	parametros.push(new ParametrosLamina(null,null,null,1,1,null,null));
	caja(m,b,0,30,-1,1,4.5,.1,-2.3,.1,null,parametros);
	caja(m,b,0,30,1,1,4.5,-.1,-2.3,.1,null,parametros);
	caja(m,b,0,30,-1,1,3.5,.1,-2.3,.1,null,parametros);
	caja(m,b,0,30,1,1,3.5,-.1,-2.3,.1,null,parametros);
	//Techo de la entrada
	lamina(m,b,0,30,null,null,null,[-1,-1.3,5],[0,-2,5],[0,-2,3.2],[-1,-1.3,3.2],1,1,null);
	lamina(m,b,0,30,null,null,null,[-.9,-1.3,5],[0,-1.9,5],[0,-1.9,3.2],[-.9,-1.3,3.2],1,1,null);
	lamina(m,b,0,30,null,null,null,[1,-1.3,5],[0,-2,5],[0,-2,3.2],[1,-1.3,3.2],1,1,null);
	lamina(m,b,0,30,null,null,null,[.9,-1.3,5],[0,-1.9,5],[0,-1.9,3.2],[.9,-1.3,3.2],1,1,null);
	//Escalas de la entrada
	caja(m,b,0,30,1,1,3.5,-2,.15,1.5,null,parametros);
	caja(m,b,0,30,1.1,1.15,3.3,-2.2,.15,1.7,null,parametros);
	//Ventanas frontales
	lamina(m,b,0,30,null,null,null,[-6,-.9,5],[-3,-.9,5],[-3,.5,5],[-6,.5,5],1,1,null);
	//Garaje
	//lamina(m,b,0,30,new Color(m,[255,255,255]),new Color(m,[0,0,0]),null,[2,-2.5,5],[2,-2.5,-2],[2,1.3,-2],[2,1.3,5],7*2,4*2,null,texturaLadrillo());
	lamina(m,b,0,30,null,null,null,[2,-2.5,5],[2,-2.5,-2],[2,1.3,-2],[2,1.3,5],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[2,-2.5,-2],[7,-2.5,-2],[7,1.3,-2],[2,1.3,-2],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[2.5,-1.5,-2.01],[6.5,-1.5,-2.01],[6.5,1.3,-2.01],[2.5,1.3,-2.01],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[2,-2.5,5],[2,-2.5,-2],[4.5,-3.5,-2],[4.5,-3.5,5],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[4.5,-3.5,5],[4.5,-3.5,-2],[7,-2.5,-2],[7,-2.5,5],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[2,-2.5,-2],[4.5,-3.5,-2],[7,-2.5,-2],[4.5,-2.5,-2],1,1,null,null);
	//Vigas del techo del patio
	caja(m,b,0,30,-2,1.3,2,-.1,-2.6,.1,null,parametros);
	caja(m,b,0,30,-7,1.3,2,.1,-2.6,.1,null,parametros);
	caja(m,b,0,30,-7,-1.3,2,5,-.1,.1,null,parametros);
	caja(m,b,0,30,-7,1.3,-2,-.1,-2.6,.1,null,parametros);
	caja(m,b,0,30,-7,-1.3,-2,-.1,-.1,4,null,parametros);
	caja(m,b,0,30,-10,1.3,-2,.1,-2.6,.1,null,parametros);
	caja(m,b,0,30,-10,-1.3,-2,2.9,-.1,.1,null,parametros);
	caja(m,b,0,30,-10,-1.3,-1.9,.1,-.1,6.9,null,parametros);
	//Techo del patio
	lamina(m,b,0,30,null,null,null,[-7,-1.4,2],[-2,-1.4,2],[-2,-3.5,5],[-8.5,-3.5,5],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[-8.5,-3.5,5],[-8.5,-3.5,0],[-7,-1.4,-2],[-7,-1.4,5],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[-8.5,-3.5,5],[-8.5,-3.5,0],[-10,-1.4,-2],[-10,-1.4,5],1,1,null,null);
	lamina(m,b,0,30,null,null,null,[-10,-1.4,-2],[-8.5,-3.5,0],[-7,-1.4,-2],[-8.5,-1.4,-2],1,1,null,null);
	//cruz
	/*c=new Linea(m,0,30,new Punto(m,0,30,[-1,0,0]),new Punto(m,0,30,[1,0,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,-1,0]),new Punto(m,0,30,[0,1,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,0,-1]),new Punto(m,0,30,[0,0,1]),false);
	b.agregar(c);*/
	//a=new Anim(m,30,[m.rad*45,m.rad*0,m.rad*0],[5,0,50]);
	a=new Anim(m,30,[m.rad*20,m.rad*0,m.rad*0],[0,-1,50]);
	b.agregarAnim(a);
	m.listaProfundidad=listaProfundidad;
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<29){
			//setTimeout(function(){dibujar(i+.01)},1);
			setTimeout(function(){dibujar(i+1)},1);
		}
	}
	dibujar(0);
}catch(e){
	console.log(e+":"+e.stack);
}
try{
	m=new MainAnim();
	//m.resx=4000;
	//m.resy=3000;
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[0,0,140]);
	b1.enlazarBase(m.e[1][0]);
	b3=new Base(m,0,300,[m.rad*180,m.rad*0,m.rad*0],[0,0,140]);
	b3.enlazarBase(m.e[1][0]);
	b2=new Base(m,0,300,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	colores[6]=new Color(m,[255,255,255]);
	colores[7]=new Color(m,[0,0,255]);
	
	//Ojo
	var r=20;
	var r1=10;
	var r2=9;
	var r3=3;
	iri(r,r1,r2,b1,colores[1],colores[1]);
	iri(r,r2,r3,b1,colores[2],colores[2]);
	iri2(r,r3,b1,colores[3],colores[3]);
	iri(r,19,15,b1);
	iri(r,15,10,b1);

	iri(r,r1,r2,b3,colores[1],colores[1]);
	iri(r,r2,r3,b3,colores[2],colores[2]);
	iri2(r,r3,b3,colores[3],colores[3]);
	iri(r,19,15,b3);
	iri(r,15,10,b3);
	
	a=new Anim(m,2,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,2,[m.rad*360,m.rad*0,m.rad*0],[0,0,140]);
	b1.agregarAnim(a);
	a=new Anim(m,2,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.agregarAnim(a);

	a=new Anim(m,2,[m.rad*(360+180),m.rad*0,m.rad*0],[0,0,140]);
	b3.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=1.99){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
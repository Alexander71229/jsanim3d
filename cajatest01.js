try{
	m=new MainAnim();
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[0,0,90]);
	b1.enlazarBase(m.e[1][0]);
	b2=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	var parametros=[];
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[0],3,3,null,texturaEjemplo02()));
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[0],3,3,null,texturaEjemplo02()));
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[0],3,3,null,texturaEjemplo02()));
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[0],3,3,null,texturaEjemplo02()));
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[0],3,3,[[1,1]],texturaEjemplo02()));
	parametros.push(new ParametrosLamina(colores[1],colores[2],colores[0],3,3,null,texturaEjemplo01()));
	caja(m,b,0,600,0,0,0,20,20,20,[1],parametros,[new ParametroCajaInterna(0,1,1,null,parametros)]);
	//caja(m,b,0,600,0,0,0,20,20,20,[1],parametros);

	//cruz
	/*c=new Linea(m,0,30,new Punto(m,0,30,[-1,0,0]),new Punto(m,0,30,[1,0,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,-1,0]),new Punto(m,0,30,[0,1,0]),false);
	b.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,0,-1]),new Punto(m,0,30,[0,0,1]),false);
	b.agregar(c);*/	

	//cruz
	/*c=new Linea(m,0,30,new Punto(m,0,30,[-1,0,0]),new Punto(m,0,30,[1,0,0]),false);
	b1.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,-1,0]),new Punto(m,0,30,[0,1,0]),false);
	b1.agregar(c);
	c=new Linea(m,0,30,new Punto(m,0,30,[0,0,-1]),new Punto(m,0,30,[0,0,1]),false);
	b1.agregar(c);*/	

	//cruz
	c=new Linea(m,0,300,new Punto(m,0,30,[-1,0,0]),new Punto(m,0,30,[1,0,0]),false);
	b2.agregar(c);
	c=new Linea(m,0,300,new Punto(m,0,30,[0,-1,0]),new Punto(m,0,30,[0,1,0]),false);
	b2.agregar(c);
	c=new Linea(m,0,300,new Punto(m,0,30,[0,0,-1]),new Punto(m,0,30,[0,0,1]),false);
	b2.agregar(c);

	
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[0,0,90]);
	b1.agregarAnim(a);
	a=new Anim(m,10,[m.rad*180,m.rad*0,m.rad*0],[0,0,0]);
	b2.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	console.log(e+":"+e.stack);
}
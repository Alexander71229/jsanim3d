try{
	m=new MainAnim();
	b=new Base(m,0,30,[m.rad*0,m.rad*0,m.rad*0],[0,0,150]);
	b.enlazarBase(m.e[1][0]);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[230,230,245]);
	colores[2]=new Color(m,[230,245,230]);
	colores[3]=new Color(m,[230,245,245]);
	colores[4]=new Color(m,[245,230,245]);
	colores[5]=new Color(m,[104,16,67]);
	//cubo
	var puntos=[];
	puntos.push(new Punto(m,0,300,[30,20,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,20,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,10,40],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[1],colores[1]);
	c.nombre='a';
	b.agregar(c);
	var puntos=[];
	puntos.push(new Punto(m,0,300,[30,10,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,10,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,40],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[2],colores[2]);
	c.nombre='b';
	b.agregar(c);
	var puntos=[];
	puntos.push(new Punto(m,0,300,[20,10,40],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,10,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[20,20,40],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[4],colores[4]);
	c.nombre='c';
	b.agregar(c);
	//Pared 1
	var puntos=[];
	puntos.push(new Punto(m,0,300,[30,-20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,-20,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	c.nombre='d';
	b.agregar(c);
	//Pared inferior
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-20,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,5,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-20,20,50],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	c.nombre='e';
	b.agregar(c);
	//Suelo
	var puntos=[];
	puntos.push(new Punto(m,0,300,[-20,20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,50],[0,0,0]));
	puntos.push(new Punto(m,0,300,[30,20,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[-20,20,0],[0,0,0]));
	c=new Poligono(m,0,3,puntos,colores[3],colores[3]);
	c.nombre='f';
	b.agregar(c);

	//Suelo
	/*var k=0;
	for(var x=-20;x<30;x=x+10){
		for(var z=0;z<50;z=z+10){
			k++;
			var puntos=[];
			puntos.push(new Punto(m,0,300,[x,20,z+10],[0,0,0]));
			puntos.push(new Punto(m,0,300,[x,20,z],[0,0,0]));
			puntos.push(new Punto(m,0,300,[x+10,20,z],[0,0,0]));
			puntos.push(new Punto(m,0,300,[x+10,20,z+10],[0,0,0]));
			if(k%2==1){
				c=new Poligono(m,0,3,puntos,colores[4],colores[5]);
			}else{
				c=new Poligono(m,0,3,puntos,colores[5],colores[4]);
			}
			b.agregar(c);
		}
	}*/
	
	
	
	//a=new Anim(m,10,[m.rad*180,m.rad*45,-m.rad*90],[0,-3,30]);
	a=new Anim(m,10,[m.rad*90,m.rad*0,m.rad*0],[30,0,150]);
	//a=new Anim(m,10,[m.rad*10,-m.rad*0,m.rad*0],[0,10,150]);
	b.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
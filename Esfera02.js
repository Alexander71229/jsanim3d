try{
	m=new MainAnim();
	m.resx=4000;
	m.resy=3000;
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[0,0,140]);
	b1.enlazarBase(m.e[1][0]);
	b2=new Base(m,0,300,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	colores[6]=new Color(m,[255,255,255]);
	colores[7]=new Color(m,[0,0,255]);
	
	//Círculo
	var r1=23;
	var r2=25;
	var tx=Math.sqrt(2)-1;
	var tg=Math.sqrt(2)/2;
	//segmento01
	var puntos=[];
	puntos.push(new Punto(m,0,300,[r2,0,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r2,tx*r2,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[tg*r2,tg*r2,0],[0,0,0]));
	puntos.push(null); //control
	puntos.push(new Punto(m,0,300,[tg*r1,tg*r1,0],[0,0,0]));
	puntos.push(new Punto(m,0,300,[r1,tx*r1,0],[0,0,0])); //control
	puntos.push(new Punto(m,0,300,[r1,0,0],[0,0,0]));
	c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	b1.agregar(c);
	
	//Esfera
	var r=10;
	var puntos=null;
	var k=0;
	var h=3;
	for(x=-r;x<=r;x+=h){
		for(y=-r;y<=r;y+=h){
			var vx=[];
			var z1=circunZ(x,y,r);
			//console.log("("+x+","+y+","+r+"):"+z1);
			if(isNaN(z1)){
				continue;
			}
			vx.push([x,y,z1]);
			var z2=circunZ(x+h,y,r);
			if(isNaN(z2)){
				continue;
			}
			vx.push([x+h,y,z2]);
			var z3=circunZ(x+h,y+h,r);
			if(isNaN(z3)){
				continue;
			}
			vx.push([x+h,y+h,z3]);
			var z4=circunZ(x,y+h,r);
			if(isNaN(z4)){
				continue;
			}
			vx.push([x,y+h,z4]);
			var puntos=[];
			puntos.push(new Punto(m,0,300,vx[0]));
			puntos.push(new Punto(m,0,300,per(vx[0],vx[1])));//control
			puntos.push(new Punto(m,0,300,vx[1]));
			puntos.push(new Punto(m,0,300,per(vx[1],vx[2])));//control
			puntos.push(new Punto(m,0,300,vx[2]));
			puntos.push(new Punto(m,0,300,per(vx[2],vx[3])));//control
			puntos.push(new Punto(m,0,300,vx[3]));
			puntos.push(new Punto(m,0,300,per(vx[3],vx[0])));//control
			puntos.push(new Punto(m,0,300,vx[0]));
			c=new CPath(m,0,3,puntos,colores[6],colores[7]);
			//console.log('ok');
			b1.agregar(c);
		}
	}
	//c=new CPath(m,0,3,puntos,colores[7],colores[7]);
	//b1.agregar(c);
	
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,10,[m.rad*90,m.rad*0,m.rad*0],[0,0,140]);
	b1.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.agregarAnim(a);
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=9){
			setTimeout(function(){dibujar(i+.01)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}
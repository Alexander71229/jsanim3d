function Punto(ani,ti,tf,v3,c){
  if(window.ais){
    ais++;
    this.aid=ais;
  }else{
    window.ais=1;
    this.aid=1;
  }
  window["elemento"+this.aid]=this;
  this.tipox="Punto";
	this.agregar=function(b){
	  this.b=b;
  }
//Proyecta un elemento de 3d a uno de 2d: mapeo de espacio
	this.mapeo=function(){
    if(this.std<2){
      if(this.t3[2]>=this.ani.zmin){
				this.v2[0]=this.ani.resx*(this.t3[0]/this.t3[2])/this.ani.visualx+this.ani.resx/2;
				this.v2[1]=this.ani.resy*(this.t3[1]/this.t3[2])/this.ani.visualy+this.ani.resy/2;
				//this.v2[0]=Math.round(this.v2[0]);
				//this.v2[1]=Math.round(this.v2[1]);
				this.std=2;
      }else{
      	this.std=3;
      }
    }
  }
	this.calcular=function(){
	  this.b.mul(this);
    for(var i=0;i<3;i++){
    	this.t3[i]=this.t3[i]+this.b.p[i];
    }
		this.zO=this.gZ();
  }
//Exportar a SVG
	this.dibujar=function(){
		return graficador.dPunto(this);
  }
	this.gD=function(){
		return Math.sqrt(this.t3[0]*this.t3[0]+this.t3[1]*this.t3[1]+this.t3[2]*this.t3[2]);
	}
	this.gZ=function(){
		return this.gD();
	}
	this.t=4;        //Tipo de elemento
	this.ani=ani;
	this.id=0;       //Identificador �nico
	this.ti=ti||0;       //Tiempo inicial
	this.tf=tf||0;       //Tiempo final
	this.d=this.tf-this.ti;        //Duraci�n
	this.b=null;        //id base
	this.v3=v3||[];      //vector de posici�n 3d
	this.v2=[];      //vector de posici�n 2d
	this.t3=this.v3;      //vector de posici�n 3d temporal: valor durante una imagen
	this.c=c||new Color(this.ani,[0,0,0]);          //Objeto Color
	this.ani.all.push(this);
}
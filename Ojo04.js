try{
	m=new MainAnim();
	//m.resx=4000;
	//m.resy=3000;
	b0=new Base(m,0,300,[m.rad*(180+90),m.rad*0,m.rad*0],[0,0,180]);
	b0.enlazarBase(m.e[1][0]);
	b1=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[25,0,0]);
	b1.enlazarBase(b0);
	b3=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-25,0,0]);
	b3.enlazarBase(b0);
	b2=new Base(m,0,300,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.enlazarBase(b1);
	b=new Base(m,0,300,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.enlazarBase(b2);
	var colores=[];
	colores[0]=new Color(m,[0,0,0]);
	colores[1]=new Color(m,[234,16,107]);
	colores[2]=new Color(m,[104,162,67]);
	colores[3]=new Color(m,[218,220,215]);
	colores[4]=new Color(m,[23,16,17]);
	colores[5]=new Color(m,[104,16,67]);
	colores[6]=new Color(m,[255,255,255]);
	colores[7]=new Color(m,[0,0,255]);
	
	//Ojo
	var r=20;
	/*var rec=10;
	var vx=[];
	vx.push(new Punto(m,0,300,[rec,-rec,circunZ(rec,-rec,r)]));
	vx.push(new Punto(m,0,300,per([rec,-rec,circunZ(rec,-rec,r)],[-rec,-rec,circunZ(-rec,-rec,r)])));
	vx.push(new Punto(m,0,300,[-rec,-rec,circunZ(-rec,-rec,r)]));
	vx.push(new Punto(m,0,300,per([-rec,-rec,circunZ(-rec,-rec,r)],[-rec,rec,circunZ(-rec,rec,r)])));
	vx.push(new Punto(m,0,300,[-rec,rec,circunZ(-rec,rec,r)]));
	vx.push(new Punto(m,0,300,per([-rec,rec,circunZ(-rec,rec,r)],[rec,rec,circunZ(rec,rec,r)])));
	vx.push(new Punto(m,0,300,[rec,rec,circunZ(rec,rec,r)]));
	vx.push(new Punto(m,0,300,per([rec,rec,circunZ(rec,rec,r)],[rec,-rec,circunZ(rec,-rec,r)])));
	vx.push(new Punto(m,0,300,[rec,-rec,circunZ(rec,-rec,r)]));
	c=new CPath(m,0,3,vx);/
	b3.agregar(c);*/
	var vx=[];
	vx.push(new Punto(m,0,300,[14,0,circunZ(14,0,r)]));
	vx.push(new Punto(m,0,300,per([14,0,circunZ(14,0,r)],[6,-10,circunZ(6,-10,r)])));
	vx.push(new Punto(m,0,300,[6,-10,circunZ(6,-10,r)]));
	vx.push(new Punto(m,0,300,per([6,-10,circunZ(6,-10,r)],[-14,-6,circunZ(-14,-6,r)])));
	vx.push(new Punto(m,0,300,[-14,-6,circunZ(-14,-6,r)]));
	vx.push(new Punto(m,0,300,per([-14,-6,circunZ(-14,-6,r)],[-7,10,circunZ(-7,10,r)])));
	vx.push(new Punto(m,0,300,[-7,10,circunZ(-7,10,r)]));
	vx.push(new Punto(m,0,300,per([-7,10,circunZ(-7,10,r)],[7,10,circunZ(7,10,r)])));
	vx.push(new Punto(m,0,300,[7,10,circunZ(7,10,r)]));
	vx.push(new Punto(m,0,300,per([7,10,circunZ(7,10,r)],[14,0,circunZ(14,0,r)])));
	vx.push(new Punto(m,0,300,[14,0,circunZ(14,0,r)]));
	c=new CPath(m,0,3,vx);
	b3.agregar(c);
	iri(r,5,1,b3);
	iri(r,8,7.9,b3);
	//iri(r,19,18,b3);

	a=new Anim(m,5,[m.rad*(90+0),m.rad*0,m.rad*0],[0,0,180]);
	b0.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-10,-10,-10]);
	b.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[25,0,0]);
	b1.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*45,-m.rad*45],[0,0,0]);
	b2.agregarAnim(a);
	a=new Anim(m,10,[m.rad*0,m.rad*0,m.rad*0],[-25,0,0]);
	b3.agregarAnim(a);
	
	function dibujar(i){
		m.calcular(i);
		m.dibujar();
		m.reset();
		if(i<m.d&&i<=5.99){
			setTimeout(function(){dibujar(i+.001)},1);
		}
	}
	dibujar(0);
}catch(e){
	alert(e+":"+e.stack);
}